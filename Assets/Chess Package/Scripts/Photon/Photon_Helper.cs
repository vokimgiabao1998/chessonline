﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class Photon_Helper
{
    /// <summary>
    /// Server
    /// </summary>
    /// <returns></returns>
    public abstract bool Check_Network();

    public abstract bool Check_Lobby_Joined();

    public abstract void Connect_To_Mater();

    public abstract void Join_Lobby();

    public abstract void On_Joied_Lobby();

    public abstract void On_Connected_State();

    public abstract void On_Diconnected_State();

    /// <summary>
    /// Room
    /// </summary>
    /// 
    public abstract bool Check_On_Close_Room();

    public abstract void Create_Random_Room();

    public abstract void Create_Custom_Room();

    public abstract void Join_Random_Room();

    public abstract void Join_Specific_Room(string roomName);

    public abstract void On_Another_Player_Entered_Room();

    public abstract void On_Room_Close();

    public abstract void On_Joined_Room(int scene_index);

    public abstract void On_Match_Scene_Finished_Loading(Scene scene, LoadSceneMode mode);

    /// <summary>
    /// Player
    /// </summary>
    public abstract void Create_Photon_Player();
}
