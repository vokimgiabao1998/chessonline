﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_Player : MonoBehaviour
{
    private PhotonView PV;
    private Photon_RPC photon_RPC;
    public static Photon_Player instance;

    public int my_team;

    private void Awake()
    {
        instance = this;
        PV = GetComponent<PhotonView>();
    }

    public void Set_Team()
    {
        photon_RPC = new Photon_RPC(PV);
        my_team = PhotonNetwork.PlayerList.Length - 1;
        photon_RPC.Send_Team(my_team);
        Photon_Debug.instance.AddDebugLog("My team is " + my_team);
    }

    [PunRPC]
    void RPC_Send_Team(int whichTeam)
    {
        my_team = whichTeam;
    }
}
