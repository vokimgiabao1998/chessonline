﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_Effect : MonoBehaviour
{
    protected PhotonView PV;
    public float time_destroy;//Đối với dead = 2; Đối với magic evolution = 4

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    private void Start()
    {
        StartCoroutine(Destroy());
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(time_destroy);
        Photon_Match_Spawning.control.Photon_Destroy_Object_Helper(this.gameObject);
    }
}
