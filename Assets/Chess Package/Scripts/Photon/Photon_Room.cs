﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Photon_Room : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    public static Photon_Room room;//Biến singleton

    private PhotonView PV;//Photon view

    private void Awake()
    {
        if (Photon_Room.room == null)
        {
            Photon_Room.room = this;
        }
        else
        {
            if (Photon_Room.room != this)
            {
                Destroy(Photon_Room.room.gameObject);
                Photon_Room.room = this;
            }
        }

        DontDestroyOnLoad(this.gameObject);
        PV = GetComponent<PhotonView>();
    }

    public override void OnEnable()
    {
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += Photon_Lobby.photon_callbacks.On_Match_Scene_Finished_Loading;
    }

    public override void OnDisable()
    {
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= Photon_Lobby.photon_callbacks.On_Match_Scene_Finished_Loading;
    }

    public override void OnJoinedRoom()
    {
        Photon_Lobby.photon_callbacks.On_Joined_Room(Photon_Var.match_scene_index);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Photon_Lobby.photon_callbacks.On_Another_Player_Entered_Room();
    }
}
