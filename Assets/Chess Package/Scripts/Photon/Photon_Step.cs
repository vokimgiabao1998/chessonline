﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Photon_Step : MonoBehaviour
{
    public static Photon_Step step;
    public Auth_Handler auth = new Auth_Handler();
    private PhotonView PV;

    public GameObject step_Prefab;
    public Transform blue_team_steps_content;
    public Transform red_team_steps_content;

    private void Awake()
    {
        step = this;
        PV = GetComponent<PhotonView>();
        step_Prefab = Resources.Load("UI Prefabs/Step") as GameObject;
    }

    private void Update()
    {
        auth.UI_Loading(step_Prefab, blue_team_steps_content, red_team_steps_content);
    }

    /// <summary>
    /// Lưu step và chuyển lên server
    /// </summary>
    public void Save_Step(int my_team, Chess_Type chess_Type, Vector3 pos)
    {
        auth.Save_Step(PV, my_team, chess_Type, pos);
    }

    /// <summary>
    /// Đổi quyền cho team khác khi kết thúc lượt
    /// </summary>
    public void Switch_Team_Permission()
    {
        auth.Update_Team_Permission(PV);
    }

    [PunRPC]
    void RPC_Send_Permission(int permission)
    {
        auth.current_team_permission = permission;
    }

    [PunRPC]
    void RPC_Save_Step(string storage)
    {
        auth.step_storage = storage;
    }
}
