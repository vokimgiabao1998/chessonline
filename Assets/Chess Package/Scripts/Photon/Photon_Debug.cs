﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Photon_Debug : MonoBehaviour
{
    public static Photon_Debug instance;
    public Transform debug_content;
    public GameObject debugTextPrefab;

    private void Awake()
    {
        instance = this;
    }

    public void AddDebugLog(string message)
    {
        GameObject deb = Instantiate(debugTextPrefab);

        deb.GetComponent<TextMeshProUGUI>().text = message;

        deb.transform.SetParent(debug_content);

        deb.transform.localScale = Vector3.one;
    }
}
