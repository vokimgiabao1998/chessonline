﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Photon_Chess_Var: Send_Helper
{
    /// <summary>
    /// Network Variables
    /// </summary>
    public int myTeam;
    public int my_Step_Count;
    public int my_Switch_Time;

    protected float velocity_speed;

    /// <summary>
    /// Action
    /// </summary>
    public bool on_destroy_another_chess;
    public bool done_attack;
    public bool on_transform_to_attack;
    public bool on_transform;
    public bool on_catch_pawn;
    public bool can_be_catched;
    public bool on_switched;
    public bool on_revolution;

    /// <summary>
    /// Vector
    /// </summary>
    public Vector3 origin_rot;

    /// <summary>
    /// Animation paramter
    /// </summary>
    public bool idle;
    public bool walk;
    public bool attack;
    public bool death;

    protected string idle_str = "idle";
    protected string walk_str = "walk";
    protected string attack_str = "attack";
    protected string death_str = "death";
    protected string revolution_str = "revolution";

    /// <summary>
    /// Enum State
    /// </summary>
    public Transform_State state;
    public Chess_Type chess_type;

    /// <summary>
    /// Attach effect
    /// </summary>
    public GameObject wanted_effect;
    public GameObject explos_effect;
}

public enum Transform_State
{
    Idle, Walk, Attack, Death
}

