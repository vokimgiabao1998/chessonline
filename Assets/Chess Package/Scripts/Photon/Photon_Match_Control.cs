﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class Photon_Match_Control : Spawn_Helper
{
    /// <summary>
    /// Gán photon_player vào match control để nhận biết player hiện tại thuộc team nào
    /// </summary>
    public void Register_Photon_Player(Photon_Player photon_player)
    {
        base.Assign_Photon_Player(photon_player);
    }

    /// <summary>
    /// Photon create network object + set team for object
    /// </summary>
    public void Spawn_Photon_Object(int team, string prefabName, Vector3 pos, Quaternion rot, byte group)
    {
        GameObject scene_object = Photon_Instantiate_Helper(prefabName, pos, rot, group);
        //Cập nhật thông tin cho scene object
        scene_object.GetComponent<Photon_Chess>().Set_Team(team);
    }

    /// <summary>
    /// Lấy tất cả script dùng để tạo chess khi mới bắt đầu game
    /// </summary>
    public Chess_SetUp[] Get_Photon_Scene_Objects()
    {
        return base.Get_Photon_Objects();
    }

    /// <summary>
    /// Dừng chờ khoảng thời gian trước khi match bắt đầu
    /// </summary>
    public override IEnumerator Yield_Wait_To_Start_Game()
    {
        Photon_Debug.instance.AddDebugLog("Wait to start game");

        yield return new WaitForSeconds(Photon_Var.time_wait_to_start_match);

        Set_Up_Scene_Object();

        Photon_Match_Spawning.instance.Enable_Match_Proccess();
    }

    /// <summary>
    /// Lấy góc xoay tùy theo team (dùng để thiết lập chess)
    /// </summary>
    public override Quaternion Get_SetUp_Rotation(Chess_SetUp scene_object)
    {
        Quaternion rotation;
        if (!scene_object.isBlueTeam)
            rotation = Quaternion.Euler(Vector3.zero);
        else rotation = Quaternion.Euler(new Vector3(0, 180, 0));
        return rotation;
    }

    /// <summary>
    /// Tạo ra network scent object = Photon
    /// </summary>
    public override void Set_Up_Scene_Object()
    {
        Photon_Debug.instance.AddDebugLog("on set up scene objects");

        foreach (Chess_SetUp scene_object in Get_Photon_Scene_Objects())
        {
            if (scene_object.isBlueTeam)
            {
                team_spawner = 0;
            }
            else team_spawner = 1;
            //
            base.Register_Square(scene_object.transform.position, team_spawner);
            //
            Spawn_Photon_Object(team_spawner, Path.Combine("Photon Character Prefabs", scene_object.chess_Type.ToString(), scene_object.chess_Type.ToString()),
                scene_object.transform.position, Get_SetUp_Rotation(scene_object), 0);
        }
    }

    /// <summary>
    /// Tìm tất cả các square có gắn tag trong game
    /// </summary>
    public override GameObject[] Get_All_Square_Object()
    {
        return GameObject.FindGameObjectsWithTag("Square");
    }

    /// <summary>
    /// Tìm tất cả scene object đã được tạo ra có gắn script Photon_Chess
    /// </summary>
    public override Photon_Chess[] Get_All_Photon_Chess()
    {
        return GameObject.FindObjectsOfType<Photon_Chess>();
    }

    /// <summary>
    /// Tìm tất cả object có gắn script Square
    /// </summary>
    public override Square[] Get_All_Square_Script()
    {
        return GameObject.FindObjectsOfType<Square>();
    }

    /// <summary>
    /// Tìm square với position cho trước
    /// </summary>
    public override Square Find_Square(Vector3 position)
    {
        if (Get_All_Square_Script().Any(m => m.transform.position == position))
        {
            return Get_All_Square_Script().Where(m => m.transform.position == position).First();
        }
        else return null;
    }

    /// <summary>
    /// Tìm chess đang đứng tại vị trí cho trước
    /// </summary>
    public override Photon_Chess Find_Chess(Vector3 position)
    {
        Photon_Chess result = null;
        foreach (Photon_Chess chess in Get_All_Photon_Chess())
        {
            x = Convert.ToInt32(chess.transform.position.x);
            y = Convert.ToInt32(chess.transform.position.y);
            z = Convert.ToInt32(chess.transform.position.z);

            if(new Vector3(x, y, z) == position)
            {
                result = chess;
                break;
            }
        }
        return result;
    }

    /// <summary>
    /// Tạo các network object square
    /// </summary>
    public override void Spawn_Square()
    {
        foreach (GameObject square in Get_All_Square_Object())
        {
            GameObject chess = Photon_Instantiate_Helper(Path.Combine("Photon Object Prefabs", "Square"), square.transform.position, square.transform.rotation, 0);
            chess.GetComponent<Square>().Un_Set_Team();
            chess.GetComponent<Square>().Sync_Square_Team();
        }
    }
}
