﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_State
{
    ///Connect to mater
    protected static bool on_connected_to_master;

    public static bool Get_Connected_Master_State()
    {
        return on_connected_to_master;
    }

    public static void On_Connected()
    {
        on_connected_to_master = true;
    }

    public static void On_Diconnected()
    {
        on_connected_to_master = false;
    }

    ///Join lobby
    protected static bool on_joined_lobby;

    public static bool Get_Joined_Lobby_State()
    {
        return on_joined_lobby;
    }

    public static void On_Join_Lobby()
    {
        on_joined_lobby = true;
    }

    ///Current Sence Index
    protected static int curent_scene;

    public static int Get_Current_Scene()
    {
        return curent_scene;
    }

    public static void Update_Scene(int scene_index)
    {
        curent_scene = scene_index;
    }

    ///Match Join Variables
    protected static bool on_join_random_match;
    protected static bool on_join_custom_match;

    public static void On_Enter_Random_Match()
    {
        on_join_random_match = true;
    }

    public static void On_Enter_Custom_Match()
    {
        on_join_custom_match = true;
    }

    public static void On_Leave_Random_Match()
    {
        on_join_random_match = false;
    }

    public static void On_Leave_Custom_Match()
    {
        on_join_custom_match = false;
    }

    ///Mtach Proccessing Variables
    public static bool on_setup_scene_object;
    public static bool on_wait_to_start_match;
    public static bool on_start_match;
    public static bool on_end_match;

    public static void On_Wait_To_Start_Match()
    {
        on_wait_to_start_match = true;
    }

    public static void On_Start_Match()
    {
        on_wait_to_start_match = false;
        on_start_match = true;
    }

    public static void On_End_Match()
    {
        on_start_match = false;
        on_end_match = true;
    }
}
