﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_RPC
{
    private PhotonView PV;

    public Photon_RPC(PhotonView photonView)
    {
        PV = photonView;
    }

    #region Photon_Chess
    public void Send_Start_Match(bool value)
    {
        PV.RPC("RPC_Send_Start_Match", RpcTarget.All, value);
    }

    public void Send_Team(int myTeam)
    {
        PV.RPC("RPC_Send_Team", RpcTarget.AllBuffered, myTeam);
    }

    public void Send_Square_Team(int myTeam)
    {
        PV.RPC("RPC_Send_Sqaure_Team", RpcTarget.AllBuffered, myTeam);
    }

    public void Send_Chess_Team(int myTeam)
    {
        PV.RPC("RPC_Send_Chess_Team", RpcTarget.AllBuffered, myTeam);
    }

    public void Send_Square_Data(bool stand)
    {
        PV.RPC("RPC_Register_Square", RpcTarget.All, stand);
    }

    public void Send_Chess_Step_Count(int count)
    {
        PV.RPC("RPC_Send_Step_Count", RpcTarget.All, count);
    }

    public void Send_Chess_Switch_Time(int count)
    {
        PV.RPC("RPC_Send_Chess_Switch_Time", RpcTarget.All, count);
    }

    public void Send_Pawn_Catch(bool catched)
    {
        PV.RPC("RPC_Pawn_Catch", RpcTarget.All, catched);
    }

    public void Send_King_Switched(bool switched)
    {
        PV.RPC("RPC_King_Switched", RpcTarget.All, switched);
    }

    public void Send_Tranform(bool transform)
    {
        PV.RPC("RPC_Send_Transform", RpcTarget.MasterClient, transform);
    }

    public void Send_Tranform_To_Attack(bool transform)
    {
        PV.RPC("RPC_Send_Transform_To_Attack", RpcTarget.MasterClient, transform);
    }

    public void Send_Target(Vector3 target)
    {
        PV.RPC("RPC_Send_Target", RpcTarget.MasterClient, target);
    }
    #endregion

    #region Match Control\
    public void Send_Match_Processing(bool proccess)
    {
        PV.RPC("RPC_Match_Proccessing", RpcTarget.All, proccess);
    }

    public void Send_Switch_King_Processing(bool proccess)
    {
        PV.RPC("RPC_Switch_King_Processing", RpcTarget.All, proccess);
    }

    public void Send_Catch_Pawn_Processing(bool proccess)
    {
        PV.RPC("RPC_Catch_Pawn_Processing", RpcTarget.All, proccess);
    }

    public void Send_Pawn_Revolution_Processing(bool proccess)
    {
        PV.RPC("RPC_Pawn_Revolution_Processing", RpcTarget.All, proccess);
    }
    #endregion

    /// <summary>
    /// Photon Step
    /// </summary>
    public void Save_Step(string steps)
    {
        PV.RPC("RPC_Save_Step", RpcTarget.All, steps);
    }

    /// <summary>
    /// Photon Auth
    /// </summary>
    public void Send_Permission(int permission)
    {
        PV.RPC("RPC_Send_Permission", RpcTarget.All, permission);
    }
}
