﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Photon_Lobby : MonoBehaviourPunCallbacks, ILobbyCallbacks
{
    public static Photon_Lobby lobby;

    public static Photon_Callbacks photon_callbacks = new Photon_Callbacks();

    private List<Photon_Room_Listing> room_listings = new List<Photon_Room_Listing>();

    private void Awake()
    {
        lobby = this;
    }

    private void Start()
    {
        if (!photon_callbacks.Check_Network())
            photon_callbacks.Connect_To_Mater();
    }

    public override void OnConnectedToMaster()//Hàm của photon nhận biết khi đã kết nối tới server
    {
        photon_callbacks.On_Connected_State();//Cập nhật tình trạng

        if (!photon_callbacks.Check_Lobby_Joined())//Kiểm tra có join lobby chưa
            photon_callbacks.Join_Lobby();
    }

    public override void OnJoinedLobby()//Hàm của photon gọi khi player đã join lobby
    {
        photon_callbacks.On_Joied_Lobby();
    }

    public override void OnDisconnected(DisconnectCause cause)//Hàm của photon nhận biết khi đã mất kết nối tới server
    {
        photon_callbacks.On_Diconnected_State();//Cập nhật tình trạng
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Tried to join random game but failed.");
        photon_callbacks.Create_Random_Room();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("Create new room failed. There must be already a room with the same name");
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (RoomInfo room in roomList)
        {
            if (room.RemovedFromList)
            {
                int index = room_listings.FindIndex(x => x.room_name == room.Name);
                if (index != -1)
                {
                    Destroy(room_listings[index].gameObject);
                    room_listings.RemoveAt(index);
                }
            }
            else ListRoom(room);
        }
    }

    private void ListRoom(RoomInfo room)//Lấy thông tin tất cả room
    {
        if (room.IsOpen && room.IsVisible)//Nếu room đó vẫn chưa đủ người hoặc chưa vào match
        {
            if (room.CustomProperties.ContainsKey(Photon_Property.RoomProperty.Mode))//Chỉ liệt kê những custom match
            {
                if (room.CustomProperties[Photon_Property.RoomProperty.Mode].Equals(Photon_Var.custom_match_index))//nếu đúng là custom match
                {
                    if (!room_listings.Any(x => x.room_name == room.Name))//Nếu đã có rồi thì không thêm vào
                    {
                        GameObject tempListing = Instantiate(Photon_UI_Delegate.instance.room_Listing_Prefab, Photon_UI_Delegate.instance.room_listing_content);
                        Photon_Room_Listing tempButton = tempListing.GetComponent<Photon_Room_Listing>();
                        tempButton.Set_Room(room.Name, room.MaxPlayers, room.PlayerCount);
                        room_listings.Add(tempButton);
                    }
                }
            }
        }
    }
}
