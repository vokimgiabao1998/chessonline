﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class Photon_Chess_Handler : Photon_Chess_Var, IMovement, ISpecialStep
{
    private Transform parent;
    private Animator anim;
    private Animation_Event_Control anim_control;
    private Click_Detecter click_detecter;
    public Photon_RPC photon_RPC;

    /// <summary>
    /// Khởi tạo tất cả thông tin liên quan của chess
    /// </summary>
    public void Set_Parent(Transform new_parent)
    {
        parent = new_parent;
        origin_rot = parent.localEulerAngles;
        anim = parent.GetComponent<Animator>();
        anim_control = new Animation_Event_Control(anim);
        click_detecter = Photon_Match_Spawning.instance.click_detecter;
        Animation_Init();
    }

    /// <summary>
    /// Khởi tạo animation event chung cho tất cả chess
    /// </summary>
    public void Animation_Init()
    {
        AnimationClip attack_clip = anim_control.Find_Clip("Attack");
        AnimationClip death_clip = anim_control.Find_Clip("Death");
        if (attack_clip != null)
        {
            int attack_index = anim_control.Get_Index(attack_clip);
            anim_control.Add_Event("Disable_Attack", attack_index, 0, attack_clip.length);
        }
        if (death_clip != null)
        {
            int death_index = anim_control.Get_Index(death_clip);
            anim_control.Add_Event("Destroying", death_index, 0, death_clip.length);
        }
    }

    /// <summary>
    /// Cập nhật tình trạng chết
    /// </summary>
    public void Death_State()
    {
        death = true;
        if (PhotonNetwork.IsMasterClient)
        {
            Spawn_Particle(parent, wanted_effect, parent.position, Quaternion.Euler(-90, 0, 0));
            Spawn_Particle(parent, explos_effect, parent.position + Vector3.up, Quaternion.Euler(-90, 0, 0));
        }
    }

    /// <summary>
    /// Cập nhật velocity khi di chuyển
    /// </summary>
    public void Apply_Velocity()
    {
        switch (chess_type)
        {
            case Chess_Type.Bishop:
                velocity_speed = 2f;
                break;
            case Chess_Type.Rook:
                velocity_speed = 6f;
                break;
            case Chess_Type.Pawn:
                velocity_speed = 2;
                break;
            case Chess_Type.King:
                velocity_speed = 1.8f;
                break;
            case Chess_Type.Queen:
                velocity_speed = 2;
                break;
            case Chess_Type.Knight:
                velocity_speed = 2;
                break;
        }
    }

    /// <summary>
    /// Xoay vế hướng target
    /// </summary>
    public void Rotate_To_Position(Vector3 dir)
    {
        Vector3 _direction = (dir - parent.transform.position);

        parent.transform.rotation = Quaternion.LookRotation(_direction);
    }

    /// <summary>
    /// Di chuyển đến vị trí cần đến
    /// </summary>
    public void Move_To_Position(Vector3 dir)
    {
        parent.transform.position = Vector3.MoveTowards(parent.transform.position, dir, Time.deltaTime * velocity_speed);
    }

    /// <summary>
    /// Quay về góc xoay ban đầu
    /// </summary>
    public void Return_To_Origin_Rotation(Quaternion rot)
    {
        parent.transform.rotation = rot;
    }

    /// <summary>
    /// Kích hoạt di chuyển đến ô trống
    /// </summary>
    public void Enable_Transform()
    {
        on_transform = true;
    }

    /// <summary>
    /// Kích hoạt di chuyển đến ô đánh
    /// </summary>
    public void Enable_Transform_To_Attack()
    {
        on_transform_to_attack = true;
    }

    /// <summary>
    /// Di chuyển đến ô trống
    /// </summary>
    public void Transform_To_Destination(Vector3 dir)
    {
        if (on_transform)//Nếu chess đang ở dạng di chuyển đến ô trống (ko di chuyển đến ô đánh)
        {
            float distance = Vector3.Distance(parent.position, dir);//Lấy khoảng cách từ chess đến ô cần đến
            if (distance >= 0.1f)
            {
                Apply_Velocity();//Cập nhật velocity
                Rotate_To_Position(dir);//Xoay theo hướng ô cần đến
                Move_To_Position(dir);//Di chuyển đến ô đó trong khoảng thời gian đã cài sẵn
                walk = true;
                idle = false;
            }
            else
            {
                //Nếu không phải nước nhập thành thì cứ đến ô mới thì cho phép tiến trình tiếp tục
                if (!Photon_Match_Spawning.instance.on_switch_king_and_rook && !Photon_Match_Spawning.instance.on_catching_pawn && !Photon_Match_Spawning.instance.on_pawn_revolution)
                    Photon_Match_Spawning.instance.Enable_Match_Proccess();//Cho phép các chess thực thi hành động của mình
                //
                on_transform = false;
                walk = false;
                idle = true;
                //Cập nhật lại thông tin ô cờ khi đã di chuyển đến ô đó
                Enter_New_Sqaure(dir);
            }
        }
    }

    /// <summary>
    /// Di chuyển đến ô cần đánh
    /// </summary>
    public void Transform_To_Attack(Vector3 dir)
    {
        if (chess_type != Chess_Type.Queen)//Tùy theo quân cờ
        {
            if (on_transform_to_attack)
            {
                Rotate_To_Position(dir);

                float distance = Vector3.Distance(parent.position, dir);
                if (distance >= 2.5f)
                {
                    Apply_Velocity();
                    parent.Translate(Vector3.forward * Time.deltaTime * velocity_speed);
                    walk = true;
                    idle = false;
                    done_attack = false;
                }
                else
                {
                    if (!done_attack)
                    {
                        walk = false;
                        idle = true;
                        attack = true;
                    }
                }
                //
                if (done_attack)
                {
                    if (Photon_Match_Spawning.instance.on_match_processing)
                    {
                        attack = false;
                        float new_distance = Vector3.Distance(parent.position, dir);
                        if (new_distance >= 0.1f)
                        {
                            Apply_Velocity();
                            Move_To_Position(dir);
                            walk = true;
                            idle = false;
                        }
                        else
                        {
                            walk = false;
                            idle = true;
                            done_attack = false;
                            on_destroy_another_chess = false;
                            on_transform_to_attack = false;
                            Enter_New_Sqaure(dir);
                        }
                    }
                }
            }
        }
        else
        {
            if (on_transform_to_attack)
            {
                if (!attack)
                {
                    attack = true;
                    Rotate_To_Position(dir);
                    //Tạo effect tại chỗ chess cần đánh
                }

                if (done_attack)
                {
                    if (Photon_Match_Spawning.instance.on_match_processing)
                    {
                        attack = false;
                        float distance = Vector3.Distance(parent.position, dir);
                        if (distance >= 0.1f)
                        {
                            Apply_Velocity();
                            Rotate_To_Position(dir);
                            Move_To_Position(dir);
                            walk = true;
                            idle = false;
                        }
                        else
                        {
                            walk = false;
                            idle = true;
                            attack = false;
                            done_attack = false;
                            on_destroy_another_chess = false;
                            on_transform_to_attack = false;
                            Enter_New_Sqaure(dir);
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Nước đi đặc biệt: bắt tốt qua đường
    /// </summary>
    public void Check_Catch_Pawn(int x, int y, int z)
    {
        Photon_Chess chess;
        if (x - 2 >= -7 && x - 2 <= 7)
        {
            chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x - 2, y, z));
            if (chess != null)
            {
                if (chess.chess_handler.chess_type == Chess_Type.Pawn)//Nếu là cùng quân pawn thì thực hiện bắt tốt
                {
                    if (chess.chess_handler.myTeam != myTeam)
                    {
                        can_be_catched = true;
                        Photon_Debug.instance.AddDebugLog("Can be catch left");
                    }
                }
            }
        }
        if (x + 2 >= -7 && x + 2 <= 7)
        {
            chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x + 2, y, z));
            if (chess != null)
            {
                if (chess.chess_handler.chess_type == Chess_Type.Pawn)//Nếu là cùng quân pawn thì thực hiện bắt tốt
                {
                    if (chess.chess_handler.myTeam != myTeam)
                    {
                        can_be_catched = true;
                        Photon_Debug.instance.AddDebugLog("Can be catch right");
                    }
                }
            }
        }
        parent.GetComponent<Photon_Chess>().photon_RPC.Send_Pawn_Catch(can_be_catched);
    }

    /// <summary>
    /// Thực thi bắt tốt qua đường(nếu có)
    /// </summary>
    public void Catch_Pawn()
    {
        int x = Convert.ToInt32(parent.transform.position.x);
        int y = Convert.ToInt32(parent.transform.position.y);
        int z = Convert.ToInt32(parent.transform.position.z);
        //
        Check_Catch_Pawn(x, y, z);
        //
        Vector3 new_dir = Vector3.zero;
        if (myTeam == 1)//Team đỏ
        {
            if (z - 2 >= -7)
            {
                new_dir = new Vector3(x, y, z - 2);//Xét vị trí sau quân pawn
            }
        }
        else
        {
            if (z + 2 <= 7)
            {
                new_dir = new Vector3(x, y, z + 2);
            }
        }
        if (new_dir != Vector3.zero)
        {
            Photon_Chess chess = Photon_Match_Spawning.control.Find_Chess(new_dir);//Nếu tìm thấy chess ở ô đó
            if (chess != null)
            {
                if (chess.chess_handler.chess_type == Chess_Type.Pawn)//Nếu là cùng quân pawn thì thực hiện bắt tốt
                {
                    if (chess.chess_handler.can_be_catched)
                    {
                        Photon_Debug.instance.AddDebugLog("Catch pawn");
                        attack = true;
                        on_catch_pawn = true;
                        //Cập nhật vị trí target
                        click_detecter.dir_pos = new_dir;
                        //Xóa thông tin ô cờ đã bị ăn
                        base.Un_Register_Square(click_detecter.dir_pos);
                        //Xoay về hướng ăn
                        Rotate_To_Position(new_dir);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Thực thi phong hậu (nếu có)
    /// </summary>
    public void Pawn_Revolution()
    {
        if (Photon_Match_Spawning.instance.on_pawn_revolution)
        {
            on_revolution = true;
            Photon_Match_Spawning.instance.Stop_Match_Proccess();
        }
    }

    /// <summary>
    /// Nước đi đặc biệt: phong hậu
    /// </summary>
    public void On_Pawn_Revolution()
    {
        //Photon Insatiate Magic effect
        if (PhotonNetwork.IsMasterClient)
        {
            base.Evolution_Effect(parent);
        }
    }

    /// <summary>
    /// Gọi khi animation chạy xong
    /// </summary>
    public void Done_Pawn_Revolution()
    {
        //disable tất cả skin của quân tốt nhưng chưa destroy
        foreach (var child in parent.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            child.gameObject.SetActive(false);
        }
        foreach (var child in parent.GetComponentsInChildren<MeshRenderer>())
        {
            child.gameObject.SetActive(false);
        }
        if (PhotonNetwork.IsMasterClient)
        {
            //Gọi hàm dừng chờ theo ý muốn
            parent.GetComponent<Photon_Chess>().Call_Wait(Wait_To_Revolution());
        }
    }

    /// <summary>
    /// Dừng chờ (tùy chọn) để spawn quân hậu và destroy quân tốt
    /// </summary>
    private IEnumerator Wait_To_Revolution()
    {
        yield return new WaitForSeconds(0.1f);
        Photon_Match_Spawning.control.Spawn_Photon_Object(myTeam, Path.Combine("Photon Character Prefabs", Chess_Type.Queen.ToString(), Chess_Type.Queen.ToString()),
            parent.transform.position, parent.transform.rotation, 0);
        Photon_Debug.instance.AddDebugLog("Done revolution");
        Photon_Match_Spawning.instance.Disable_Pawn_Revolution_Proccess();
        Photon_Match_Spawning.instance.Enable_Match_Proccess();
        Photon_Match_Spawning.control.Photon_Destroy_Object_Helper(parent.gameObject);
    }

    /// <summary>
    /// Nước đi đặc biệt: nhập thành
    /// </summary>
    public void Enable_King_Switch()
    {
        on_switched = true;
    }

    /// <summary>
    /// Thực thi nhập thành vua và xe(nếu có)
    /// </summary>
    public void Switch_King_And_Rook()
    {
        if (Photon_Match_Spawning.instance.on_switch_king_and_rook)
        {
            x = Convert.ToInt32(parent.transform.position.x);
            y = Convert.ToInt32(parent.transform.position.y);
            z = Convert.ToInt32(parent.transform.position.z);
            Vector3 ches_dir;
            Vector3 new_dir;
            if (x > 0)
            {
                new_dir = new Vector3(1, y, z);
                ches_dir = new Vector3(7, y, z);
            }
            else
            {
                new_dir = new Vector3(-3, y, z);
                ches_dir = new Vector3(-7, y, z);
            }
            Photon_Chess rook_chess = Photon_Match_Spawning.control.Find_Chess(ches_dir);
            if (rook_chess != null)
            {
                //bỏ chọn ô của quân rook
                base.Un_Register_Square(ches_dir);

                //Send vị trí ô cần đến đến master client
                base.Send_Target(photon_RPC, Photon_Match_Spawning.instance.PV, new_dir);

                //Cập nhật số nước đi cho quân cờ hiện tại
                base.Increase_Step(photon_RPC, rook_chess.PV, rook_chess.chess_handler.my_Step_Count + 1);

                //Send biến cho phép di chuyển đến master client
                base.Enable_Transform(photon_RPC, rook_chess.gameObject);
            }
        }
    }

    /// <summary>
    /// Cập nhật thông tin khi đến ô mới
    /// </summary>
    public void Enter_New_Sqaure(Vector3 dir)
    {
        //Cập nhật lại postion và rotaion khi đến square mới
        parent.transform.localPosition = dir;
        Return_To_Origin_Rotation(Quaternion.Euler(origin_rot));

        //Cập nhật thông tin cho ô đi đến
        base.Register_Square(dir, myTeam);

        if (PhotonNetwork.IsMasterClient)
        {
            //Clear Step Storage
            Photon_Match_Spawning.instance.click_detecter.Reset_Step_Storage();
            //Save ô vửa đi xong qua class Photon_Step
            Photon_Step.step.Save_Step(myTeam, chess_type, dir);

            //Switch team permission
            if (!Photon_Match_Spawning.instance.on_switch_king_and_rook)//Nếu ko phải nước nhập thành thì switch permission cho người chơi kia
                Photon_Step.step.Switch_Team_Permission();
            else//Còn nếu đang nhập thành thì phải đợi quân xe di chuyển đến ô mới mới chuyển quần cho người chơi kia
            {
                if (chess_type == Chess_Type.Rook)
                {
                    Photon_Step.step.Switch_Team_Permission();
                    Photon_Match_Spawning.instance.Disable_Switch_King_Proccess();
                    Photon_Match_Spawning.instance.Enable_Match_Proccess();
                }
            }
        }

        if (chess_type == Chess_Type.Pawn)
        {
            //Nước đi đặc biệt: bắt tốt qua đường
            Catch_Pawn();
            //Nước đi đặc biệt: phong hậu
            Pawn_Revolution();
        }

        //Nước đi đặc biệt: nhập thành
        if (chess_type == Chess_Type.King)
            Switch_King_And_Rook();
    }

    /// <summary>
    /// Quản lý animation của character
    /// </summary>
    public void Handle_Animation()
    {
        anim.SetBool(idle_str, idle);
        anim.SetBool(walk_str, walk);
        anim.SetBool(attack_str, attack);
        anim.SetBool(death_str, death);
        anim.SetBool(revolution_str, on_revolution);
    }

    /// <summary>
    /// Quản lý trạng thái của character
    /// </summary>
    public void Handle_State()
    {
        if (idle)
        {
            state = Transform_State.Idle;
        }
        else if (walk)
        {
            state = Transform_State.Walk;
        }
        else if (attack)
        {
            state = Transform_State.Attack;
        }
    }
}
