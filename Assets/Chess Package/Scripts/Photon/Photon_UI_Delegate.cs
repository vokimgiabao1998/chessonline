﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Photon_UI_Delegate : MonoBehaviour
{
    public static Photon_UI_Delegate instance;

    //Scrollview content
    public Transform room_listing_content;

    //Prefab
    public GameObject room_Listing_Prefab;

    //UI
    public Button random_match_button;
    public Button custom_match_button;
    public TMP_InputField roomNameInput;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        random_match_button.onClick.AddListener(delegate () { Photon_Event_Click.instance.On_Join_Random_Game(); });
        custom_match_button.onClick.AddListener(delegate () { Photon_Event_Click.instance.On_Join_Custom_Game(); });
    }

    public void Disable_Random_Match_Button()
    {
        random_match_button.enabled = false;
    }

    public void Disable_Custom_Match_Button()
    {
        custom_match_button.enabled = false;
    }
}
