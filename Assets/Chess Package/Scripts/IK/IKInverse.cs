﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKInverse : MonoBehaviour
{
    public Animator anim;

    public bool ikActive = true;
    public GameObject parent;

    public Transform left_foot;
    public Transform right_foot;
    public Transform left_knee;
    public Transform right_knee;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        anim.SetBool("death", parent.GetComponent<Photon_Chess>().chess_handler.death);
    }

    void OnAnimatorIK()
    {
        if (ikActive)
        {
            if (left_foot != null && right_foot != null)
            {
                anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, 1);
                anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, 1);
                //
                anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, 1);
                anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, 1);
                //
                anim.SetIKPosition(AvatarIKGoal.LeftFoot, left_foot.position);
                anim.SetIKPosition(AvatarIKGoal.RightFoot, right_foot.position);
                ///
                anim.SetIKRotation(AvatarIKGoal.LeftFoot, left_foot.rotation);
                anim.SetIKRotation(AvatarIKGoal.RightFoot, right_foot.rotation);
            }

            if (left_knee != null && right_knee != null)
            {
                anim.SetIKHintPositionWeight(AvatarIKHint.LeftKnee, 1);
                anim.SetIKHintPositionWeight(AvatarIKHint.RightKnee, 1);
                ///
                anim.SetIKHintPosition(AvatarIKHint.RightKnee, right_knee.position);
                anim.SetIKHintPosition(AvatarIKHint.LeftKnee, left_knee.position);
            }
        }
    }
}
