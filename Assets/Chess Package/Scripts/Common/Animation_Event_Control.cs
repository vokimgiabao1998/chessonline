﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Animation_Event_Control : Animation_Event_Helper
{
    private Animator myAnim;

    public Animation_Event_Control(Animator anim)
    {
        myAnim = anim;
    }

    public override AnimationClip Find_Clip(string name)
    {
        if (myAnim.runtimeAnimatorController.animationClips.Any(m => m.name.Contains(name)))
        {
            return myAnim.runtimeAnimatorController.animationClips.Where(m => m.name.Contains(name)).Single();
        }
        else return null;
    }

    public override AnimationClip Get_Clip(int index)
    {
        return myAnim.runtimeAnimatorController.animationClips[index];
    }

    public override int Get_Index(AnimationClip clip)
    {
        return myAnim.runtimeAnimatorController.animationClips.Select((s, i) => new { i, s }).Where(m => m.s == clip).Select(t => t.i).Single();
    }

    public override bool Check_Event(string function_name, int index, int param, float time)
    {
        if (Get_Clip(index).events.Any(m => m.functionName == function_name && m.time == time && m.intParameter == param))
            return true;
        else return false;
    }

    public override void Add_Event(string function_name, int index, int param, float time)
    {
        AnimationClip myClip = Get_Clip(index);

        AnimationEvent anim_event = new AnimationEvent();

        anim_event.functionName = function_name;

        anim_event.time = time;

        myClip.AddEvent(anim_event);
    }

}
