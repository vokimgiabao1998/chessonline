﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Click_Detecter: Step_Helper, ISpecialStep
{
    public Photon_RPC photon_RPC;
    public Vector3 dir_pos;
    private RaycastHit hit;
    //
    //
    public override void Prev_Step_Init()//Khai báo 4 list tạm tượng trưng có 4 danh sách ô chéo xung quanh quân cờ
    {
        one_prev = new List<Vector3>();
        two_prev = new List<Vector3>();
        three_prev = new List<Vector3>();
        four_prev = new List<Vector3>();
    }

    public override GameObject Click_On_Chess()//Nhận dạng quân cờ nào đang được click trên bàn cờ
    {
        GameObject chess_on_click = null;
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);//Chiếu 1 tia từ camera tới thẳng chỗ mouse position
            if(Physics.Raycast(ray, out hit, Mathf.Infinity, Photon_Var.chess_mask))//Xét click đối vs những object nào là quân cờ
            {
                if(hit.collider != null && hit.transform.GetComponent<Photon_Chess>() != null)//Nếu object dc click là quân cờ
                {
                    if (hit.transform.GetComponent<Photon_Chess>().chess_handler.myTeam == Photon_Step.step.auth.current_team_permission
                        && Photon_Step.step.auth.current_team_permission == Photon_Match_Spawning.instance.photon_player.my_team
                        && Photon_Match_Spawning.instance.on_match_processing)
                    {
                        //Photon_Debug.instance.AddDebugLog("I click on " + hit.transform.name);
                        chess_on_click = hit.transform.gameObject;//Trả về quân cờ đang dc click
                        current_chess = chess_on_click;
                        //Refresh hiệu ứng của tất cả ô đang select
                        Refresh_Select();
                        //Hiện effect tại ô mà chess được click
                        x = Convert.ToInt32(current_chess.transform.position.x);
                        y = Convert.ToInt32(current_chess.transform.position.y);
                        z = Convert.ToInt32(current_chess.transform.position.z);
                        Show_Select(new Vector3(x, y, z));
                    }
                    else
                    {
                        current_chess = null;
                        Refresh_Select();
                        Refresh_Steps();
                    }
                }
            }
        }
        return chess_on_click;
    }

    public override bool Check_Step(Vector3 dir)//Kiểm tra quân cờ tại vị trí đang xét có phải cùng quân với quân cờ đang click hay ko? (nếu cùng quân thì trả về true)
    {
        if (Photon_Match_Spawning.control.Get_All_Square_Script().Any(m => m.transform.position == dir))//Kiểm tra xem vị trí đang xét có nằm trong phạm vi bàn cờ hay ko?
        {
            Square square = Photon_Match_Spawning.control.Find_Square(dir);//Tìm ô tại vị trí đang xét
            if (square.myTeam == Click_On_Chess().GetComponent<Photon_Chess>().chess_handler.myTeam)//Nếu cùng quân thì trả về true
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }

    public override bool Check_Exist(Vector3 dir)//Kiểm tra ô đang xét hiện có quân cờ nào đang đứng hay ko?
    {
        if (Photon_Match_Spawning.control.Get_All_Square_Script().Any(m => m.transform.position == dir))//Kiểm tra xem vị trí đang xét có nằm trong phạm vi bàn cờ hay ko?
        {
            Square square = Photon_Match_Spawning.control.Find_Square(dir);//Tìm ô tại vị trí đang xét
            if (square.is_Stand)//Nếu có quân cờ đang đứng thì trả về true
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else return false;
    }

    public override void Delete_Prev(List<Vector3> destinations, List<Vector3> prev)//Xóa các nước đi dư, chỉ trừ lại nước đi ăn quân đầu tiên
    {
        for (int i = prev.Count - 1; i > 0; i--)
        {
            destinations.Remove(prev[i]);
        }
    }

    public override Vector3 Choose_Step(List<Vector3> steps)//Khi click vào quân cờ sẽ hiện ra các nước đi, khi chuột hover vào 1 trong các nước đi cho phép sẽ hiện lên ô muốn đi tiếp
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, Photon_Var.square_mask))
        {
            if (hit.collider != null)
            {
                foreach (var step in steps)
                {
                    Square sqaure = Photon_Match_Spawning.control.Find_Square(step);
                    if (sqaure != null)
                    {
                        if (step == hit.transform.position)
                        {
                            sqaure.Choose_Step();

                            if (Input.GetMouseButtonDown(0))
                            {
                                if (current_chess != null && Photon_Match_Spawning.instance.on_match_processing)
                                {
                                    ///Cập nhật thông tin cho ô hiện tại
                                    x = Convert.ToInt32(current_chess.transform.position.x);
                                    y = Convert.ToInt32(current_chess.transform.position.y);
                                    z = Convert.ToInt32(current_chess.transform.position.z);

                                    //Dừng tất cả hoạt động liên quan cho tới khi từng chess thực thi xong hành động của mình
                                    Photon_Match_Spawning.instance.Stop_Match_Proccess();

                                    //Refresh tất cả các ô đang chọn và các ô có thể đi
                                    Refresh_Select();
                                    Refresh_Steps();

                                    //Bỏ chọn ô hiện tại
                                    Un_Register_Square(new Vector3(x, y, z));

                                    //Gán vị trí cần đến
                                    dir_pos = step;

                                    //Send vị trí ô cần đến đến master client
                                    Send_Target(photon_RPC, Photon_Match_Spawning.instance.PV, dir_pos);

                                    //Cập nhật số nước đi cho quân cờ hiện tại
                                    Increase_Step(photon_RPC, current_chess.GetComponent<Photon_Chess>().PV, current_chess.GetComponent<Photon_Chess>().chess_handler.my_Step_Count + 1);

                                    if (!sqaure.is_Stand)
                                    {
                                        base.Enable_Transform(photon_RPC, current_chess);
                                    }
                                    else
                                    {
                                        if (current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam != Photon_Match_Spawning.control.Find_Chess(dir_pos).chess_handler.myTeam)
                                        {
                                            base.Enable_Transform_To_Attack(photon_RPC, current_chess);
                                        }
                                    }

                                    //Kiểm tra nước nhập thành: nếu dc thì cho phép nhập thành
                                    if (base.Check_Switch_Pos(current_chess, Convert.ToInt32(sqaure.transform.position.x))
                                        && current_chess.GetComponent<Photon_Chess>().chess_handler.my_Switch_Time < 1)
                                    {
                                        Photon_Match_Spawning.instance.Enable_Switch_King_Proccess();
                                        base.Enable_Switch_King_And_Rook(photon_RPC, current_chess);
                                    }

                                    //Kiểm tra nước bắt tốt qua đường: nếu dc thì cho bắt tốt
                                    if(base.Check_Pawn_Catch_Pos(current_chess, x, z, Convert.ToInt32(sqaure.transform.position.x), Convert.ToInt32(sqaure.transform.position.z)))
                                    {
                                        if (!Check_Exist(sqaure.transform.position))//Nếu vị trí đang tới là ô trống (ko ăn quân ở ô đang tới thì cho bắt tốt)
                                        {
                                            Photon_Match_Spawning.instance.Enable_Catch_Pawn_Proccess();
                                        }
                                    }

                                    if(base.Check_Pawn_Revolution(current_chess, Convert.ToInt32(sqaure.transform.position.z)))
                                    {
                                        Photon_Debug.instance.AddDebugLog("I am on pawn revolution");
                                        Pawn_Revolution();
                                    }
                                }
                            }
                        }
                        else sqaure.Un_Choose_Step();
                    }
                }
            }
        }
        return dir_pos;
    }

    public void Catch_Pawn()//Lưu lại các nước có thể bắt tốt qua đường
    {
        x = Convert.ToInt32(current_chess.transform.position.x);
        y = Convert.ToInt32(current_chess.transform.position.y);
        z = Convert.ToInt32(current_chess.transform.position.z);

        x1 = x + 2;
        z1 = z + 2;
        ///
        x2 = x - 2;
        z2 = z - 2;

        Photon_Chess chess;

        if (current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam == 1)
        {
            if (x1 >= -7 && x1 <= 7 && z1 >= -7 && z1 <= 7)
            {
                chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x1, y, z));
                if (chess != null)
                {
                    if (chess.chess_handler.chess_type == Chess_Type.Pawn && chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                    {
                        if (base.Check_Catch_Pawn(chess))
                        {
                            steps_storage.Add(new Vector3(x1, y, z1));
                        }
                    }
                }
            }

            if (x2 >= -7 && x2 <= 7 && z1 >= -7 && z1 <= 7)
            {
                chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x2, y, z));
                if (chess != null)
                {
                    if (chess.chess_handler.chess_type == Chess_Type.Pawn && chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                    {
                        if (Check_Catch_Pawn(chess))
                        {
                            steps_storage.Add(new Vector3(x2, y, z1));
                        }
                    }
                }
            }
        }
        else
        {
            if (x1 >= -7 && x1 <= 7 && z2 >= -7 && z2 <= 7)
            {
                chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x1, y, z));
                if (chess != null)
                {
                    if (chess.chess_handler.chess_type == Chess_Type.Pawn && chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                    {
                        if (Check_Catch_Pawn(chess))
                        {
                            steps_storage.Add(new Vector3(x1, y, z2));
                        }
                    }
                }
            }

            if (x2 >= -7 && x2 <= 7 && z2 >= -7 && z2 <= 7)
            {
                chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x2, y, z));
                if (chess != null)
                {
                    if (chess.chess_handler.chess_type == Chess_Type.Pawn && chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                    {
                        if (Check_Catch_Pawn(chess))
                        {
                            steps_storage.Add(new Vector3(x2, y, z2));
                        }
                    }
                }
            }
        }
    }

    public void Pawn_Revolution()//Kích hoạt tiến trình phong hậu
    {
        Photon_Match_Spawning.instance.Enable_Pawn_Revolution_Proccess();
    }

    public void Switch_King_And_Rook()//Lưu lại các nước có thể nhập thành
    {
        x = Convert.ToInt32(current_chess.transform.position.x);
        y = Convert.ToInt32(current_chess.transform.position.y);
        z = Convert.ToInt32(current_chess.transform.position.z);

        if (!base.Check_Switched(current_chess))
        {
            if (!base.Check_Mate(current_chess, x, y, z))
            {
                Photon_Debug.instance.AddDebugLog("Can Switch king and rook");

                if (Check_King_Close(current_chess, x, y, z))
                    steps_storage.Add(new Vector3(x - 4, y, z));
                if (Check_King_Far(current_chess, x, y, z))
                    steps_storage.Add(new Vector3(x + 4, y, z));
            }
        }
        else Photon_Debug.instance.AddDebugLog("You only switch one time");
    }

    public override List<Vector3> Pawn_Step(Vector3 current_pos)//Tính nước đi của quân tốt
    {
        Prev_Step_Init();
        List<Vector3> destinations = new List<Vector3>();
        //Đầu tiên đổi các trục x, y, z về giá trị chuẩn
        x = Convert.ToInt32(current_pos.x);
        y = Convert.ToInt32(current_pos.y);
        z = Convert.ToInt32(current_pos.z);
        //
        if (current_chess.GetComponent<Photon_Chess>().chess_handler.my_Step_Count == 0)//Nếu quân tốt hiện đang ở nước đi đầu thì cho phép đi 2 ô phía trước
        {
            if (x >= -7 && x <= 7 && z >= -7 && z <= 7)
            {
                if (current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam == 1)//Nếu là team red
                {
                    if (!Check_Step(new Vector3(x, y, z + 2)))//Nếu ô đầu tiên ko bị chặn
                    {
                        if (!Check_Exist(new Vector3(x, y, z + 2)))//Nếu có vật cản ngay lập tức dừng
                        {
                            destinations.Add(new Vector3(x, y, z + 2));

                            if (x >= -7 && x <= 7 && z >= -7 && z <= 7)
                            {
                                if (!Check_Step(new Vector3(x, y, z + 4)))
                                {
                                    if (!Check_Exist(new Vector3(x, y, z + 4)))//Nếu có vật cản ngay lập tức dừng
                                    {
                                        //Photon_Debug.instance.AddDebugLog("x: " + x + "; z: " + z + 4);
                                        destinations.Add(new Vector3(x, y, z + 4));//Lưu lại các nước đi cho phép
                                    }
                                }
                            }
                        }
                    }
                }
                else if(current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam == 0)//Nếu là team xanh
                {
                    if (!Check_Step(new Vector3(x, y, z - 2)))//Nếu ô đầu tiên ko bị chặn
                    {
                        if (!Check_Exist(new Vector3(x, y, z - 2)))//Nếu có vật cản ngay lập tức dừng
                        {
                            destinations.Add(new Vector3(x, y, z - 2));

                            if (x >= -7 && x <= 7 && z >= -7 && z <= 7)
                            {
                                if (!Check_Step(new Vector3(x, y, z - 4)))
                                {
                                    if (!Check_Exist(new Vector3(x, y, z - 4)))//Nếu có vật cản ngay lập tức dừng
                                    {
                                        //Photon_Debug.instance.AddDebugLog("x: " + x + "; z: " + z + 4);
                                        destinations.Add(new Vector3(x, y, z - 4));//Lưu lại các nước đi cho phép
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else//Nếu là nước đi thứ 2 thì chỉ cho phép đi 1 ô phía trước
        {
            if (x >= -7 && x <= 7 && z >= -7 && z <= 7)
            {
                if (current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam == 1)//Nếu là team red
                {
                    if (!Check_Step(new Vector3(x, y, z + 2)))
                    {
                        if (!Check_Exist(new Vector3(x, y, z + 2)))//Nếu có vật cản ngay lập tức dừng
                        {
                            //Photon_Debug.instance.AddDebugLog("x: " + x + "; z: " + z + 2);
                            destinations.Add(new Vector3(x, y, z + 2));//Lưu lại các nước đi cho phép
                        }
                    }
                }
                else if (current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam == 0)//Nếu là team xanh
                {
                    if (!Check_Step(new Vector3(x, y, z - 2)))
                    {
                        if (!Check_Exist(new Vector3(x, y, z - 2)))//Nếu có vật cản ngay lập tức dừng
                        {
                            //Photon_Debug.instance.AddDebugLog("x: " + x + "; z: " + z + 2);
                            destinations.Add(new Vector3(x, y, z - 2));//Lưu lại các nước đi cho phép
                        }
                    }
                }
            }
        }
        //Xét các ô chéo xung quanh
        int[] X = new int[2] { 2, -2 };
        int[] Z = new int[2] { 2,  2 };
        //
        for (int i = 0; i < 2; i++)
        {
            if (current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam == 1)//Nếu là team red
            {
                float u = x + X[i];
                float v = z + Z[i];
                if (u >= -7 && u <= 7 && v >= -7 && v <= 7)
                {
                    if (!Check_Step(new Vector3(u, y, v)))
                    {
                        if (Check_Exist(new Vector3(u, y, v)))
                        {
                            if (!destinations.Exists(m => m.x == u && m.z == v))
                            {
                                //Photon_Debug.instance.AddDebugLog("x: " + u + "; z: " + v);
                                destinations.Add(new Vector3(u, y, v));//Lưu lại các nước đi cho phép
                            }
                        }
                    }
                }
            }
            else if (current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam == 0)//Nếu là team red
            {
                float u = x + X[i];
                float v = z - Z[i];
                if (u >= -7 && u <= 7 && v >= -7 && v <= 7)
                {
                    if (!Check_Step(new Vector3(u, y, v)))
                    {
                        if (Check_Exist(new Vector3(u, y, v)))
                        {
                            if (!destinations.Exists(m => m.x == u && m.z == v))
                            {
                                //Photon_Debug.instance.AddDebugLog("x: " + u + "; z: " + v);
                                destinations.Add(new Vector3(u, y, v));//Lưu lại các nước đi cho phép
                            }
                        }
                    }
                }
            }
        }
        return destinations;
    }

    public override List<Vector3> Knight_Step(Vector3 current_pos)//Tính nước đi của quân mã
    {
        List<Vector3> destinations = new List<Vector3>();
        //Liệt kê các nước đi cho phép xung quanh quân mã
        int[] X = new int[8] { 2, -2, -2, 2, -4, 4, -4, 4 };
        int[] Z = new int[8] { 4, 4, -4, -4, 2, 2, -2, -2 };

        for (int i = 0; i < 8; i++)
        {
            u = Convert.ToInt32(current_pos.x) + X[i];
            v = Convert.ToInt32(current_pos.z) + Z[i];
            y = Convert.ToInt32(current_pos.y);

            if (u >= -7 && u <= 7 && v >= -7 && v <= 7)//Xét trong phạm vi bàn cờ
            {
                if (!Check_Step(new Vector3(u, y, v)))//Nếu ô ko bị chặn
                {
                    if (!destinations.Exists(m => m.x == u && m.z == v))//Bỏ qua ô đã xét
                    {
                        //Photon_Debug.instance.AddDebugLog("x: " + u + "; z: " + v);
                        destinations.Add(new Vector3(u, y, v));//Lưu lại các nước đi cho phép
                    }
                }
            }
        }
        return destinations;
    }

    public override List<Vector3> Bishop_Step(Vector3 current_pos)//Tính nước đi của quân tượng
    {
        Prev_Step_Init();//Khởi tạo danh sách các nc đi
        List<Vector3> destinations = new List<Vector3>();
        int multiplier = 0;
        bool continued_one = true, continued_two = true, continued_three = true, continued_four = true;//4 biến tượng trưng cho 4 lượt xét chéo xung quanh quân cờ

        for (int i = 0; i < 8; i++)
        {
            multiplier++;
            x1 = Convert.ToInt32(current_pos.x) + multiplier * 2;
            z1 = Convert.ToInt32(current_pos.z) + multiplier * 2;
            ///
            x2 = Convert.ToInt32(current_pos.x) - multiplier * 2;
            z2 = Convert.ToInt32(current_pos.z) - multiplier * 2;
            ///
            y = Convert.ToInt32(current_pos.y);
            ///
            if (continued_one)
            {
                if (x1 >= -7 && x1 <= 7 && z1 >= -7 && z1 <= 7)
                {
                    if (!Check_Step(new Vector3(x1, y, z1)))
                    {
                        if (Check_Exist(new Vector3(x1, y, z1)))//Nếu có vật cản ngay lập tức dừng
                        {
                            continued_one = false;
                            one_prev.Add(new Vector3(x1, y, z1));//Lưu lại ô có thể đánh
                        }
                        if (!destinations.Exists(m => m.x == x1 && m.z == z1))//Bỏ qua ô đã xét
                        {
                            //Photon_Debug.instance.AddDebugLog("One x: " + x1 + "; z: " + z1);
                            destinations.Add(new Vector3(x1, y, z1));
                        }
                    }
                    else
                    {
                        continued_one = false;
                    }
                }
            }
            //
            if (continued_two)
            {
                if (x2 >= -7 && x2 <= 7 && z1 >= -7 && z1 <= 7)
                {
                    if (!Check_Step(new Vector3(x2, y, z1)))
                    {
                        if (Check_Exist(new Vector3(x2, y, z1)))
                        {
                            continued_two = false;
                            two_prev.Add(new Vector3(x2, y, z1));
                        }
                        if (!destinations.Exists(m => m.x == x2 && m.z == z1))
                        {
                            //Photon_Debug.instance.AddDebugLog("Two x: " + x2 + "; z: " + z1);
                            destinations.Add(new Vector3(x2, y, z1));
                        }
                    }
                    else continued_two = false;
                }
            }
            //
            if (continued_three)
            {
                if (x2 >= -7 && x2 <= 7 && z2 >= -7 && z2 <= 7)
                {
                    if (!Check_Step(new Vector3(x2, y, z2)))
                    {
                        if (Check_Exist(new Vector3(x2, y, z2)))
                        {
                            continued_three = false;
                            three_prev.Add(new Vector3(x2, y, z2));
                        }
                        if (!destinations.Exists(m => m.x == x2 && m.z == z2))
                        {
                            //Photon_Debug.instance.AddDebugLog("Three x: " + x2 + "; z: " + z2);
                            destinations.Add(new Vector3(x2, y, z2));
                        }
                    }
                    else continued_three = false;
                }
            }
            //
            if (continued_four)
            {
                if (x1 >= -7 && x1 <= 7 && z2 >= -7 && z2 <= 7)
                {
                    if (!Check_Step(new Vector3(x1, y, z2)))
                    {
                        if (Check_Exist(new Vector3(x1, y, z2)))
                        {
                            continued_four = false;
                            four_prev.Add(new Vector3(x1, y, z2));
                        }
                        if (!destinations.Exists(m => m.x == x1 && m.z == z2))
                        {
                            //Photon_Debug.instance.AddDebugLog("Four x: " + x1 + "; z: " + z2);
                            destinations.Add(new Vector3(x1, y, z2));
                        }
                    }
                    else continued_four = false;
                }
            }
            //Xóa các nước đi dư, chỉ chừa lại ô có thể đánh tương ứng với 4 vị trí chéo xung quanh
            Delete_Prev(destinations, one_prev);
            Delete_Prev(destinations, two_prev);
            Delete_Prev(destinations, three_prev);
            Delete_Prev(destinations, four_prev);
        }
        return destinations;
    }

    public override List<Vector3> Rook_Step(Vector3 current_pos)//Xử lý tương tự như quân tượng
    {
        Prev_Step_Init();
        List<Vector3> destinations = new List<Vector3>();
        int multiplier = 0;
        bool continued_one = true, continued_two = true, continued_three = true, continued_four = true;

        for (int i = 0; i < 8; i++)
        {
            multiplier++;
            x1 = Convert.ToInt32(current_pos.x) + multiplier * 2;
            z1 = Convert.ToInt32(current_pos.z) + multiplier * 2;
            ///
            x2 = Convert.ToInt32(current_pos.x) - multiplier * 2;
            z2 = Convert.ToInt32(current_pos.z) - multiplier * 2;
            ///
            x = Convert.ToInt32(current_pos.x);
            y = Convert.ToInt32(current_pos.y);
            z = Convert.ToInt32(current_pos.z);

            if (continued_one)
            {
                if (z1 >= -7 && z1 <= 7)
                {
                    if (!Check_Step(new Vector3(x, y, z1)))
                    {
                        if (Check_Exist(new Vector3(x, y, z1)))
                        {
                            continued_one = false;
                            one_prev.Add(new Vector3(x, y, z1));
                        }
                        if (!destinations.Exists(m => m.x == x && m.z == z1))
                        {
                            //Photon_Debug.instance.AddDebugLog("One x: " + x + "; z: " + z1);
                            destinations.Add(new Vector3(x, y, z1));
                        }
                    }
                    else continued_one = false;
                }
            }
            if (continued_two)
            {
                if (z2 >= -7 && z2 <= 7)
                {
                    if (!Check_Step(new Vector3(x, y, z2)))
                    {
                        if (Check_Exist(new Vector3(x, y, z2)))
                        {
                            continued_two = false;
                            two_prev.Add(new Vector3(x, y, z2));
                        }
                        if (!destinations.Exists(m => m.x == x && m.z == z2))
                        {
                            //Photon_Debug.instance.AddDebugLog("Two x: " + x + "; z: " + z2);
                            destinations.Add(new Vector3(x, y, z2));
                        }
                    }
                    else continued_two = false;
                }
            }
            if (continued_three)
            {
                if (x1 >= -7 && x1 <= 7)
                {
                    if (!Check_Step(new Vector3(x1, y, z)))
                    {
                        if (Check_Exist(new Vector3(x1, y, z)))
                        {
                            continued_three = false;
                            three_prev.Add(new Vector3(x1, y, z));
                        }
                        if (!destinations.Exists(m => m.x == x1 && m.z == z))
                        {
                            //Photon_Debug.instance.AddDebugLog("Three x: " + x1 + "; z: " + z);
                            destinations.Add(new Vector3(x1, y, z));
                        }
                    }
                    else continued_three = false;
                }
            }
            if (continued_four)
            {
                if (x2 >= -7 && x2 <= 7)
                {
                    if (!Check_Step(new Vector3(x2, y, z)))
                    {
                        if (Check_Exist(new Vector3(x2, y, z)))
                        {
                            continued_four = false;
                            four_prev.Add(new Vector3(x2, y, z));
                        }
                        if (!destinations.Exists(m => m.x == x2 && m.z == z))
                        {
                            //Photon_Debug.instance.AddDebugLog("One x: " + x2 + "; z: " + z);
                            destinations.Add(new Vector3(x2, y, z));
                        }
                    }
                    else continued_four = false;
                }
            }
            Delete_Prev(destinations, one_prev);
            Delete_Prev(destinations, two_prev);
            Delete_Prev(destinations, three_prev);
            Delete_Prev(destinations, four_prev);
        }
        return destinations;
    }

    public override List<Vector3> Queen_Step(Vector3 current_pos)//Kết hợp nước đi của xe và tượng
    {
        List<Vector3> destinations = new List<Vector3>();
        foreach (var rook_step in Rook_Step(current_pos))
        {
            destinations.Add(rook_step);
        }
        foreach (var bishop_step in Bishop_Step(current_pos))
        {
            destinations.Add(bishop_step);
        }
        return destinations;
    }

    public override List<Vector3> King_Step(Vector3 current_pos)//Tương tự như quân hậu nhưng chỉ xét 1 ô
    {
        List<Vector3> destinations = new List<Vector3>();
        int[] X = new int[4] { 2, -2, -2,  2 };
        int[] Z = new int[4] { 2, -2,  2, -2 };
        //
        x = Convert.ToInt32(current_pos.x);
        y = Convert.ToInt32(current_pos.y);
        z = Convert.ToInt32(current_pos.z);
        //
        for (int i = 0; i < 4; i++)
        {
            float u = x + X[i];
            float v = z + Z[i];
            if (u >= -7 && u <= 7 && v >= -7 && v <= 7)
            {
                if (!Check_Step(new Vector3(u, y, v)))
                {
                    if (!destinations.Exists(m => m.x == u && m.z == v))
                    {
                        //Photon_Debug.instance.AddDebugLog("x: " + u + "; z: " + v);
                        destinations.Add(new Vector3(u, y, v));
                    }
                }
            }
        }
        ///
        if (!Check_Step(new Vector3(x, y, z + 2)))
        {
            if (!destinations.Exists(m => m.x == x && m.z == z + 2))
            {
                //Photon_Debug.instance.AddDebugLog("One x: " + x + "; z: " + (z + 2));
                destinations.Add(new Vector3(x, y, z + 2));
            }
        }
        if (!Check_Step(new Vector3(x + 2, y, z)))
        {
            if (!destinations.Exists(m => m.x == x + 2 && m.z == z))
            {
                //Photon_Debug.instance.AddDebugLog("One x: " + (x + 2) + "; z: " + z);
                destinations.Add(new Vector3(x + 2, y, z));
            }
        }
        if (!Check_Step(new Vector3(x - 2, y, z)))
        {
            if (!destinations.Exists(m => m.x == x - 2 && m.z == z))
            {
                //Photon_Debug.instance.AddDebugLog("One x: " + (x - 2) + "; z: " + z);
                destinations.Add(new Vector3(x - 2, y, z));
            }
        }
        if (!Check_Step(new Vector3(x, y, z + 2)))
        {
            if (!destinations.Exists(m => m.x == x && m.z == z + 2))
            {
                //Photon_Debug.instance.AddDebugLog("One x: " + x + "; z: " + (z + 2));
                destinations.Add(new Vector3(x, y, z + 2));
            }
        }
        if (!Check_Step(new Vector3(x, y, z - 2)))
        {
            if (!destinations.Exists(m => m.x == x && m.z == z - 2))
            {
                //Photon_Debug.instance.AddDebugLog("One x: " + x + "; z: " + (z - 2));
                destinations.Add(new Vector3(x, y, z - 2));
            }
        }
        return destinations;
    }

    public override void Show_Select(Vector3 pos)//Hiện hiệu ứng ô đang click
    {
        Square square = Photon_Match_Spawning.control.Find_Square(pos);
        if (square != null)
            square.Select_Step();
    }

    public override void Show_Step(List<Vector3> positions)//Hiện các ô có thể đi
    {
        foreach (Square square in Photon_Match_Spawning.control.Get_All_Square_Script())
        {
            if (positions.Exists(m => m == square.transform.position))
            {
                Square _sq = Photon_Match_Spawning.control.Find_Square(square.transform.position);
                _sq.Show_Step();
            }
            else square.Hide_Step();
        }
    }

    public override void Refresh_Select()//Bỏ chọn tất cả ô đang click
    {
        foreach (Square square in Photon_Match_Spawning.control.Get_All_Square_Script())
        {
            square.Un_Select_Step();
        }
    }

    public override void Refresh_Steps()//Ẩn tất cả ô có thể đi
    {
        foreach (Square square in Photon_Match_Spawning.control.Get_All_Square_Script())
        {
            square.Hide_Step();
        }
    }

    public override void Reset_Step_Storage()//Xóa tất cả các nước đi đã lưu trong list storage
    {
        steps_storage.Clear();
    }

    public override Chess_Type Get_Chess_Type(GameObject chess)//Lấy kiểu chess
    {
        return chess.GetComponent<Photon_Chess>().chess_handler.chess_type;
    }

    public override void Step_Control()//Quản lý tất cả các bước chọn, di chuyển của chess
    {
        if (Photon_Match_Spawning.instance.on_match_processing)
        {
            GameObject chess_on_click = Click_On_Chess();
            ///Hiện ô có thể đi
            if (chess_on_click != null)
            {
                Vector3 current_pos = chess_on_click.transform.position;
                //
                Chess_Type chess_type = Get_Chess_Type(chess_on_click);
                //
                switch (chess_type)
                {
                    case Chess_Type.Pawn:
                        steps_storage = Pawn_Step(current_pos);
                        Catch_Pawn();
                        Show_Step(steps_storage);
                        break;
                    case Chess_Type.Knight:
                        steps_storage = Knight_Step(current_pos);
                        Show_Step(steps_storage);
                        break;
                    case Chess_Type.Bishop:
                        steps_storage = Bishop_Step(current_pos);
                        Show_Step(steps_storage);
                        break;
                    case Chess_Type.Rook:
                        steps_storage = Rook_Step(current_pos);
                        Show_Step(steps_storage);
                        break;
                    case Chess_Type.Queen:
                        steps_storage = Queen_Step(current_pos);
                        Show_Step(steps_storage);
                        break;
                    case Chess_Type.King:
                        steps_storage = King_Step(current_pos);
                        Switch_King_And_Rook();
                        Show_Step(steps_storage);
                        break;
                }
            }

            ///Hiện ô muốn đánh
            if (steps_storage.Count != 0)
            {
                Choose_Step(steps_storage);
            }
        }
    }
}
