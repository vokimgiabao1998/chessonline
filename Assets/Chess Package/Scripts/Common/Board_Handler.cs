﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board_Handler: Board_Helper
{
    public void Board_SetUp()
    {
        base.Board_Matrix_SetUp();
    }

    public void Board_Debug_Test()
    {
        base.Board_Debug();
    }

    public string Get_Step(Vector3 pos)
    {
        return base.Get_Step_Value(pos);
    }
}
