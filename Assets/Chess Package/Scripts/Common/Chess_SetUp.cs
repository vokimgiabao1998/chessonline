﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chess_SetUp : MonoBehaviour
{
    public bool isBlueTeam;

    public Chess_Type chess_Type;
}

public enum Chess_Type
{
    Pawn, Rook, Bishop, Knight, Queen, King
}
