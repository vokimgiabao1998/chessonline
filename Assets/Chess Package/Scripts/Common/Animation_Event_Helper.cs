﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Animation_Event_Helper
{
    public abstract AnimationClip Find_Clip(string name);

    public abstract AnimationClip Get_Clip(int index);

    public abstract int Get_Index(AnimationClip clip);

    public abstract void Add_Event(string function_name, int index, int param, float time);

    public abstract bool Check_Event(string function_name, int index, int param, float time);
}
