﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Step_Helper: Send_Helper
{
    public abstract GameObject Click_On_Chess();

    public abstract Chess_Type Get_Chess_Type(GameObject chess);

    public abstract void Step_Control();

    public abstract void Prev_Step_Init();

    public abstract Vector3 Choose_Step(List<Vector3> destioations);

    public abstract bool Check_Step(Vector3 dir);

    public abstract bool Check_Exist(Vector3 dir);

    public abstract void Delete_Prev(List<Vector3> destinations, List<Vector3> prev);

    public abstract void Show_Select(Vector3 pos);

    public abstract void Show_Step(List<Vector3> positions);

    public abstract void Refresh_Select();

    public abstract void Refresh_Steps();

    public abstract void Reset_Step_Storage();

    public abstract List<Vector3> Pawn_Step(Vector3 current_pos);

    public abstract List<Vector3> Knight_Step(Vector3 current_pos);

    public abstract List<Vector3> Bishop_Step(Vector3 current_pos);

    public abstract List<Vector3> Rook_Step(Vector3 current_pos);

    public abstract List<Vector3> Queen_Step(Vector3 current_pos);

    public abstract List<Vector3> King_Step(Vector3 current_pos);
}
