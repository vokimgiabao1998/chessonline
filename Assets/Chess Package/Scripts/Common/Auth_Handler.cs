﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

[System.Serializable]
public class Auth_Handler : Auth_Helper
{
    protected Photon_RPC photon_RPC;

    public override void Update_Team_Permission(PhotonView PV)
    {
        if (current_team_permission == 0) current_team_permission = 1;
        else if (current_team_permission == 1) current_team_permission = 0;
        //
        photon_RPC = new Photon_RPC(PV);
        photon_RPC.Send_Permission(current_team_permission);
    }

    public override void Refresh_Steps(Transform blue_team_steps_content, Transform red_team_steps_content)
    {
        //Xóa tất cả child UI của team blue
        foreach (Transform child in blue_team_steps_content.GetComponentsInChildren<Transform>())
        {
            if (child != blue_team_steps_content)
                MonoBehaviour.Destroy(child.gameObject);
        }
        //Xóa tất cả child UI của team red
        foreach (Transform child in red_team_steps_content.GetComponentsInChildren<Transform>())
        {
            if (child != red_team_steps_content)
                MonoBehaviour.Destroy(child.gameObject);
        }
    }

    public override void Save_Step(PhotonView PV, int my_team, Chess_Type chess_Type, Vector3 pos)
    {
        base.step_storage += my_team + "/";
        base.step_storage += (byte)chess_Type + "/";
        base.step_storage += (int)pos.x + "/";
        base.step_storage += (int)pos.y + "/";
        base.step_storage += (int)pos.z + "/";
        base.step_storage += "*";

        photon_RPC = new Photon_RPC(PV);
        base.Send_Step_Storage(photon_RPC, PV, step_storage);
    }

    public override void UI_SetUp(GameObject step_prefab, Transform blue_team_steps_content, Transform red_team_steps_content, int team, int type, int x, int y, int z)
    {
        if (team == 0)
            base.step_ui = MonoBehaviour.Instantiate(step_prefab, blue_team_steps_content);
        else base.step_ui = MonoBehaviour.Instantiate(step_prefab, red_team_steps_content);

        if (base.step_ui != null)
            base.step_ui.GetComponent<TextMeshProUGUI>().text = (Chess_Type)type + ": " + Board_SetUp.board.board_Handler.Get_Step(new Vector3(x, y, z));
    }

    public override void UI_Loading(GameObject step_prefab, Transform blue_team_steps_content, Transform red_team_steps_content)
    {
        if (base.step_storage.Length != base.prev_length)
        {
            Refresh_Steps(blue_team_steps_content, red_team_steps_content);
            //
            base.prev_length = base.step_storage.Length;
            //
            string[] steps = base.step_storage.Split('*');
            //
            foreach (string val in steps.Where(i => !string.IsNullOrEmpty(i)))
            {
                string[] s = val.Split('/');
                //
                for (int i = 0; i < s.Length; i++)
                {
                    if (s[i] != null && s[i] != "" && s[i] != " ")
                    {
                        base.count++;
                        if (base.count == 1) base.team = Convert.ToInt32(s[i]);
                        if (base.count == 2) base.chess_type = Convert.ToInt32(s[i]);
                        if (base.count == 3) base.x = Convert.ToInt32(s[i]);
                        if (base.count == 4) base.y = Convert.ToInt32(s[i]);
                        if (base.count == 5) base.z = Convert.ToInt32(s[i]);
                    }
                }
                UI_SetUp(step_prefab, blue_team_steps_content, red_team_steps_content, base.team, base.chess_type, base.x, base.y, base.z);
                base.count = 0;
            }
        }
    }
}
