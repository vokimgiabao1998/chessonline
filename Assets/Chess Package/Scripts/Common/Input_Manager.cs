﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Input_Manager
{
    public static KeyCode Controls_Key { get; protected set; }
    public static KeyCode Help_Key { get; protected set; }
    public static KeyCode Menu_Key { get; protected set; }

    protected static string Controls_Key_Name = "controls_key";
    protected static string Help_Key_Name = "help_key";
    protected static string Menu_Key_Name = "menu_key";

    protected static KeyCode Root_Control_Key = KeyCode.Z;
    protected static KeyCode Root_Help_Key = KeyCode.Space;
    protected static KeyCode Root_Menu_Key = KeyCode.Escape;

    public static void Load_All_Key()
    {
        //Control key
        if (Save_Data.Check_Key_Data(Controls_Key_Name))
            Controls_Key = (KeyCode)System.Enum.Parse(typeof(KeyCode), Save_Data.Get_Key_Data(Controls_Key_Name));
        else
            Save_Data.Save_Key_Data(Controls_Key_Name, Root_Control_Key.ToString());
        
        //Help key
        if (Save_Data.Check_Key_Data(Help_Key_Name))
            Help_Key = (KeyCode)System.Enum.Parse(typeof(KeyCode), Save_Data.Get_Key_Data(Help_Key_Name));
        else
            Save_Data.Save_Key_Data(Help_Key_Name, Root_Help_Key.ToString());

        //Menu key
        if (Save_Data.Check_Key_Data(Menu_Key_Name))
            Menu_Key = (KeyCode)System.Enum.Parse(typeof(KeyCode), Save_Data.Get_Key_Data(Menu_Key_Name));
        else
            Save_Data.Save_Key_Data(Menu_Key_Name, Root_Menu_Key.ToString());
    }
}
