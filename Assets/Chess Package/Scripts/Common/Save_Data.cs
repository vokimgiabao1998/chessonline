﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Save_Data
{
    public static void Delete_All_Keys()
    {
        PlayerPrefs.DeleteAll();
    }

    public static bool Check_Key_Data(string keyName)
    {
        if (PlayerPrefs.HasKey(keyName))
            return true;
        else return false;
    }

    public static void Save_Key_Data(string keyName, string value)
    {
        PlayerPrefs.SetString(keyName, value);
    }

    public static string Get_Key_Data(string keyName)
    {
       return PlayerPrefs.GetString(keyName);
    }
}
