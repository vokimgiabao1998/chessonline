﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Board_Helper
{
    protected int number = 0;
    protected int multiplier = 2;
    protected int x = 7, y = 7;
    protected string symbol = "";
    protected Dictionary<Vector3, string> board_matrix = new Dictionary<Vector3, string>();

    protected string Board_Symbol(int j)
    {
        switch (j)
        {
            case 7:
                symbol = "A";
                break;
            case 5:
                symbol = "B";
                break;
            case 3:
                symbol = "C";
                break;
            case 1:
                symbol = "D";
                break;
            case -1:
                symbol = "E";
                break;
            case -3:
                symbol = "F";
                break;
            case -5:
                symbol = "G";
                break;
            case -7:
                symbol = "H";
                break;
        }
        return symbol;
    }

    protected void Board_Matrix_SetUp()
    {
        for (int i = x; i >= -7; i-= multiplier)
        {
            for (int j = y; j >= -7; j-= multiplier)
            {
                number++;
                board_matrix.Add(new Vector3(i, 0, j), (Board_Symbol(i) + number).ToString());
            }
            number = 0;
        }
    }

    protected string Get_Step_Value(Vector3 key_pos)
    {
        return board_matrix[key_pos];
    }

    protected void Board_Debug()
    {
        foreach (var item in board_matrix)
        {
            Debug.Log("Pos: " + item.Key + "/ Name: " + item.Value);
        }
    }
}
