﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Tab : MonoBehaviour
{
    private Button tab_button;
    private TextMeshProUGUI tab_name_text;
    public int control_index;
    public bool is_origin;
    private float origin_text_size;

    private void Awake()
    {
        tab_button = GetComponent<Button>();
        tab_name_text = GetComponentInChildren<TextMeshProUGUI>();
        origin_text_size = tab_name_text.fontSize;
        if (!is_origin)
            None_State();
        else Click_State();
    }

    private void Start()
    {
        tab_button.onClick.AddListener(delegate { On_Tab_Click(); });
    }

    private void On_Tab_Click()
    {
        Click_State();
        TabManager.manager.tab_Controls[control_index].tab_Group.Show_Page(this);
        TabManager.manager.tab_Controls[control_index].tab_Group.Un_Choose_Other_Tabs(this);
    }

    public void None_State()
    {
        tab_name_text.color = Color.black;
        if (tab_button.transform.childCount > 1)
        {
            tab_button.transform.GetChild(0).GetComponent<Image>().enabled = true;
            tab_button.transform.GetChild(1).GetComponent<Image>().enabled = false;
        }
    }

    private void Click_State()
    {
        tab_name_text.color = Color.white;
        Scale_Text_On_Click();
        if (tab_button.transform.childCount > 1)
        {
            tab_button.transform.GetChild(0).GetComponent<Image>().enabled = false;
            tab_button.transform.GetChild(1).GetComponent<Image>().enabled = true;
        }
    }

    public void Un_Scale_Text()
    {
        tab_name_text.fontSize = origin_text_size;
    }

    private void Scale_Text_On_Click()
    {
        StartCoroutine(Text_Scale_Time(origin_text_size - 7));
    }

    IEnumerator Text_Scale_Time(float size)
    {
        while (tab_name_text.fontSize > size)
        {
            tab_name_text.fontSize -= 1;

            yield return null;
        }
    }
}
