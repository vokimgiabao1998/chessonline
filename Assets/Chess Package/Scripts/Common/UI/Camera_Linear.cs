﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Linear : MonoBehaviour
{
    public Transform root;
    public Transform dir;
    public float speed;
    public float smooth;
    public bool on_change;

    private void Update()
    {
        if (Vector3.Distance(transform.position, dir.position) > 4f && !on_change)
        {
            on_change = false;
        }
        else
        {
            if (Vector3.Distance(transform.position, root.position) > 4f)
                on_change = true;
            else if (on_change)
                on_change = false;
        }

        if (on_change)
        {
            transform.position = Vector3.Lerp(transform.position, root.position, Time.deltaTime * speed / smooth);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, dir.position, Time.deltaTime * speed / smooth);
        }
    }
}
