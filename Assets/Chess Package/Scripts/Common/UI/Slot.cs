﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
    private Image enable_image;
    private Image click_image;
    private Vector3 scale = new Vector3(1.035f, 1.035f, 1.035f);
    public int control_index;
    private bool inited;

    private void Awake()
    {
        enable_image = transform.GetChild(0).GetComponent<Image>();
        click_image = transform.GetChild(1).GetComponent<Image>();
        transform.localScale = Vector3.zero;
    }

    void OnEnable()
    {
        StartCoroutine(ScaleOverTime(0.5f));
    }

    IEnumerator ScaleOverTime(float time)
    {
        Vector3 originalScale = transform.localScale;
        Vector3 destinationScale = Vector3.one;
        float currentTime = 0.0f;
        do
        {
            transform.localScale = Vector3.Lerp(originalScale, destinationScale, currentTime / time);
            currentTime += Time.deltaTime;
            yield return null;
        } 
        while (currentTime <= time);
        inited = true;
        Normal_Slot();
    }

    public void Normal_Slot()
    {
        transform.localScale = Vector3.one;
        enable_image.enabled = true;
        click_image.enabled = false;
    }

    public void Hover_Slot()
    {
        transform.localScale = scale;
        enable_image.enabled = false;
        click_image.enabled = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (inited)
        {
            Hover_Slot();
            Tab_Control.control.tab_Group.Un_Choose_Other_Slots(this);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (inited)
        {
            Normal_Slot();
            Tab_Control.control.tab_Group.Un_Choose_Other_Slots(this);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (inited)
        {
            Debug.Log("on slot click");
        }
    }
}
