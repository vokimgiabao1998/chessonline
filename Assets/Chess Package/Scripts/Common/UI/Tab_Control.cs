﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tab_Control : MonoBehaviour
{
    public static Tab_Control control;
    public Tab_Group tab_Group;
    public Transform pages_parent;

    private void Awake()
    {
        control = this;
        tab_Group = new Tab_Group(transform, pages_parent);
    }
}
