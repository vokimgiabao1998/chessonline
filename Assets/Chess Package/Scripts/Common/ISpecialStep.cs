﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpecialStep
{
    void Catch_Pawn();

    void Pawn_Revolution();

    void Switch_King_And_Rook();
}
