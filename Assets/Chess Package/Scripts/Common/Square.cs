﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square : MonoBehaviour
{
    public bool is_Stand;

    public bool is_Choose;

    public bool is_Select;

    public bool is_Step_Show;

    public int myTeam;

    public GameObject step;

    public GameObject pos;

    public PhotonView PV;

    private Photon_RPC photon_RPC;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        photon_RPC = new Photon_RPC(PV);
    }

    private void Update()
    {
        if (is_Step_Show)
        {
            step.SetActive(true);
        }
        else step.SetActive(false);

        if (is_Select)
        {
            pos.SetActive(true);
        }
        else pos.SetActive(false);

        if (is_Choose)
        {
            step.GetComponent<SpriteRenderer>().color = Color.green;
        }
        else
        {
            step.GetComponent<SpriteRenderer>().color = Color.yellow;
        }

        if (is_Select)
        {
            pos.GetComponent<SpriteRenderer>().color = Color.yellow;
        }
    }

    public void Select_Step()
    {
        is_Select = true;
    }

    public void Un_Select_Step()
    {
        is_Select = false;
    }

    public void Choose_Step()
    {
        is_Choose = true;
    }

    public void Un_Choose_Step()
    {
        is_Choose = false;
    }

    public void Show_Step()
    {
        is_Step_Show = true;
    }

    public void Hide_Step()
    {
        is_Step_Show = false;
    }

    public void Register_Square()
    {
        is_Stand = true;
    }

    public void Un_Register_Square()
    {
        is_Stand = false;
    }

    public void Set_Team(int team)
    {
        myTeam = team;
    }

    public void Un_Set_Team()
    {
        myTeam = -1;
    }

    public void Sync_Square_Data()
    {
        photon_RPC.Send_Square_Data(is_Stand);
    }

    public void Sync_Square_Team()
    {
        photon_RPC.Send_Square_Team(myTeam);
    }

    [PunRPC]
    void RPC_Register_Square(bool standed)
    {
        is_Stand = standed;
    }

    [PunRPC]
    void RPC_Send_Sqaure_Team(int team)
    {
        myTeam = team;
    }
}
