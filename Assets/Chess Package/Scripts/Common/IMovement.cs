﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovement
{
    void Animation_Init();

    void Apply_Velocity();

    void Handle_Animation();

    void Handle_State();

    void Enable_Transform();

    void Enable_Transform_To_Attack();

    void Move_To_Position(Vector3 dir);

    void Rotate_To_Position(Vector3 dir);

    void Return_To_Origin_Rotation(Quaternion rot);

    void Enter_New_Sqaure(Vector3 dir);
}
