﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Send_Helper: Effect_Helper
{
    public GameObject current_chess;
    protected List<Vector3> steps_storage = new List<Vector3>();
    protected List<Vector3> one_prev;
    protected List<Vector3> two_prev;
    protected List<Vector3> three_prev;
    protected List<Vector3> four_prev;
    protected int x, y, z, u, v, x1, x2, z1, z2;

    #region Click_Detecter
    /// <summary>
    /// Bỏ chọn ô hiện tại
    /// </summary>
    protected void Un_Register_Square(Vector3 dir)
    {
        //Tìm ô với vị trí đang xét
        Square current_sqaure = Photon_Match_Spawning.control.Find_Square(dir);
        current_sqaure.Un_Register_Square();
        current_sqaure.Sync_Square_Data();
        //
        current_sqaure.Un_Set_Team();
        current_sqaure.Sync_Square_Team();
    }

    /// <summary>
    /// Chọn ô hiện tại
    /// </summary>
    protected void Register_Square(Vector3 dir, int myTeam)
    {
        //Tìm ô với vị trí đang xét
        Square sqaure = Photon_Match_Spawning.control.Find_Square(dir);
        sqaure.Register_Square();
        sqaure.Sync_Square_Data();
        //
        sqaure.Set_Team(myTeam);
        sqaure.Sync_Square_Team();
    }

    /// <summary>
    /// Gửi vị trí ô cần đến qua server
    /// </summary>
    protected void Send_Target(Photon_RPC photon_RPC, PhotonView PV, Vector3 dir_pos)
    {
        photon_RPC = new Photon_RPC(PV);
        photon_RPC.Send_Target(dir_pos);
    }

    /// <summary>
    /// Cộng số nước đi
    /// </summary>
    protected void Increase_Step(Photon_RPC photon_RPC, PhotonView PV, int count)
    {
        photon_RPC = new Photon_RPC(PV);
        photon_RPC.Send_Chess_Step_Count(count);
    }

    /// <summary>
    /// Gửi biến cho phép di chuyển đến ô trống qua server
    /// </summary>
    protected void Enable_Transform(Photon_RPC photon_RPC, GameObject current_chess)
    {
        //Cập nhật biến transform
        current_chess.GetComponent<Photon_Chess>().chess_handler.Enable_Transform();
        //Send biến cho phép di chuyển đến master client
        photon_RPC = new Photon_RPC(current_chess.GetComponent<Photon_Chess>().PV);
        photon_RPC.Send_Tranform(current_chess.GetComponent<Photon_Chess>().chess_handler.on_transform);
    }

    /// <summary>
    /// Gửi biến cho phép di chuyển đến ô cần đánh qua server
    /// </summary>
    protected void Enable_Transform_To_Attack(Photon_RPC photon_RPC, GameObject current_chess)
    {
        //Cập nhật biến transform to attack
        current_chess.GetComponent<Photon_Chess>().chess_handler.Enable_Transform_To_Attack();
        //Send biến cho phép di chuyển đến master client
        photon_RPC = new Photon_RPC(current_chess.GetComponent<Photon_Chess>().PV);
        photon_RPC.Send_Tranform_To_Attack(current_chess.GetComponent<Photon_Chess>().chess_handler.on_transform_to_attack);
    }

    /// <summary>
    /// Gửi biến cho phép nhập thành king và rook qua server
    /// </summary>
    protected void Enable_Switch_King_And_Rook(Photon_RPC photon_RPC, GameObject current_chess)
    {
        Photon_Debug.instance.AddDebugLog("Rook can switch");
        //
        photon_RPC = new Photon_RPC(current_chess.GetComponent<Photon_Chess>().PV);
        photon_RPC.Send_Chess_Switch_Time(current_chess.GetComponent<Photon_Chess>().chess_handler.my_Switch_Time + 1);
        //
        photon_RPC = new Photon_RPC(current_chess.GetComponent<Photon_Chess>().PV);
        current_chess.GetComponent<Photon_Chess>().chess_handler.Enable_King_Switch();
        photon_RPC.Send_King_Switched(current_chess.GetComponent<Photon_Chess>().chess_handler.on_switched);
    }


    /// <summary>
    /// Các nước đi đặc biệt
    /// </summary>
    /// <returns></returns>
    /// 
    public bool Check_Pawn_Revolution(GameObject current_chess, int z)
    {
        if (current_chess.GetComponent<Photon_Chess>().chess_handler.chess_type == Chess_Type.Pawn)
        {
            if (z == -7 || z == 7)
                return true;
            else return false;
        }
        else return false;
    }
    
    //Xét vị trí tốt nằm ở hàng ngang thứ 5 thì mới cho bắt tốt
    public bool Check_Catch_Pawn(Photon_Chess chess)
    {
        bool allowed = false;
        int z = Convert.ToInt32(chess.transform.position.z);
        if (chess.chess_handler.can_be_catched)//Chỉ bắt tốt khi quân tốt đi lần 1 (có nghĩa là nhảy 2 ô khi đi lần đầu)
        {
            if (chess.chess_handler.my_Step_Count == 1)
            {
                if (z == 1 || z == -1)//Chỉ cho bắt tốt qua đường khi ở hàng ngang số 5(tính theo mỗi bên)
                {
                    allowed = true;
                }
            }
        }
        return allowed;
    }

    //Kiểm tra vị trí sau của vị trí đang click có tốt đang đứng ko? Nếu có thì cho bắt
    public bool Check_Pawn_Catch_Pos(GameObject current_chess, int x, int z, int x1, int z1)
    {
        Photon_Chess chess;
        if (current_chess.GetComponent<Photon_Chess>().chess_handler.chess_type == Chess_Type.Pawn)
        {
            if ((Math.Abs(z1 - z) == 2 && 2 == Math.Abs(x1 - x)) || (Math.Abs(z + z1) == 2 && 2 == Math.Abs(x1 + x)))
            {
                if (z1 - 2 >= -7 && z1 - 2 <= 7)
                {
                    if (Photon_Match_Spawning.instance.click_detecter.Check_Exist(new Vector3(x1, y, z1 - 2)))
                    {
                        chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x1, y, z1 - 2));
                        if (chess != null)
                        {
                            if (chess.chess_handler.chess_type == Chess_Type.Pawn)//Nếu là cùng quân pawn thì thực hiện bắt tốt
                            {
                                if (chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                if (z1 + 2 >= -7 && z1 + 2 <= 7)
                {
                    if (Photon_Match_Spawning.instance.click_detecter.Check_Exist(new Vector3(x1, y, z1 + 2)))
                    {
                        chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x1, y, z1 + 2));
                        if (chess != null)
                        {
                            if (chess.chess_handler.chess_type == Chess_Type.Pawn)//Nếu là cùng quân pawn thì thực hiện bắt tốt
                            {
                                if (chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    //Kiểm tra vị trí click ô có phải rơi vào đúng vị trí quân vua di chuyển để nhập thành hay ko?
    protected bool Check_Switch_Pos(GameObject current_chess, int x)
    {
        if (current_chess.GetComponent<Photon_Chess>().chess_handler.chess_type == Chess_Type.King)
        {
            if (x == -5 || x == 3)
            {
                return true;
            }
        }
        return false;
    }

    //Kiểm tra quân vua đã thực hiện nhập thành hay chưa?
    protected bool Check_Switched(GameObject current_chess)
    {
        if (current_chess.GetComponent<Photon_Chess>().chess_handler.chess_type == Chess_Type.King)
        {
            if (current_chess.GetComponent<Photon_Chess>().chess_handler.on_switched)
            {
                return true;
            }
        }
        return false;
    }

    //Check đường nhập thành gần
    protected bool Check_King_Close(GameObject current_chess, int x, int y, int z)
    {
        bool allowed = true;
        Photon_Chess chess;
        if (current_chess.GetComponent<Photon_Chess>().chess_handler.my_Step_Count == 0)
        {
            //Xét đường nhập thành theo luật cờ vua
            for (int i = x - 2; i >= -5; i -= 2)
            {
                chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(i, y, z));
                if (chess != null)
                {
                    allowed = false;
                    break;
                }
                if (!Check_Knight_Surround(current_chess, i, y, z))
                {
                    allowed = false;
                    break;
                }
                if (!Check_Vertical(current_chess, i, y, z))
                {
                    allowed = false;
                    break;
                }
                if (!Check_Surround(current_chess, i, y, z))
                {
                    allowed = false;
                    break;
                }
            }
        }
        else allowed = false;
        return allowed;
    }

    //Check đường nhập thành xa
    protected bool Check_King_Far(GameObject current_chess, int x, int y, int z)
    {
        bool allowed = true;
        Photon_Chess chess;
        if (current_chess.GetComponent<Photon_Chess>().chess_handler.my_Step_Count == 0)
        {
            //Xét đường nhập thành theo luật cờ vua
            for (int i = x + 2; i <= 5; i += 2)
            {
                chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(i, y, z));
                if (chess != null)
                {
                    allowed = false;
                    break;
                }
                if (!Check_Knight_Surround(current_chess, i, y, z))
                {
                    allowed = false;
                    break;
                }
                if (!Check_Vertical(current_chess, i, y, z))
                {
                    allowed = false;
                    break;
                }
                if (!Check_Surround(current_chess, i, y, z))
                {
                    allowed = false;
                    break;
                }
            }
        }
        else allowed = false;
        return allowed;
    }

    //Kiểm tra có bị chiếu tướng hay không?
    protected bool Check_Mate(GameObject current_chess, int x, int y, int z)
    {
        bool check_mate = false;
        //Bổ sung hàm xét quân vua hiện ko bị chiếu
        if (!Check_Knight_Surround(current_chess, x, y, z))
        {
            check_mate = true;
            Photon_Debug.instance.AddDebugLog("knight chess mate");
        }
        if (!check_mate)
        {
            if (!Check_Vertical(current_chess, x, y, z))
            {
                check_mate = true;
                Photon_Debug.instance.AddDebugLog("vertical chess mate");
            }
        }
        if (!check_mate)
        {
            if (!Check_Surround(current_chess, x, y, z))
            {
                check_mate = true;
                Photon_Debug.instance.AddDebugLog("Surround chess mate");
            }
        }
        return check_mate;
    }

    //Check tất cả các quân theo chiều dọc (bỏ qua quân tốt => vì tốt di chuyển 1 ô nên xét ở hàm khác, mã, tượng vì những quân này ko di chuyển theo chiều dọc nên ko thể bị chiếu)
    protected bool Check_Vertical(GameObject current_chess, int x, int y, int z)
    {
        bool allowed = true;
        int multiplier = 0;

        for (int i = 0; i < 8; i++)
        {
            multiplier++;
            float z1 = z + multiplier * 2;
            float z2 = z - multiplier * 2;
            ///
            if (z1 <= 7)
            {
                Photon_Chess chess_b = Photon_Match_Spawning.control.Find_Chess(new Vector3(x, y, z1));
                if (chess_b != null)
                {
                    if (chess_b.chess_handler.myTeam == current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                    {
                        break;
                    }
                    else
                    {
                        if (chess_b.chess_handler.chess_type != Chess_Type.Pawn && chess_b.chess_handler.chess_type != Chess_Type.Knight && chess_b.chess_handler.chess_type != Chess_Type.Bishop)
                            allowed = false;
                        else if (chess_b.chess_handler.chess_type == Chess_Type.Pawn)
                        {
                            Photon_Debug.instance.AddDebugLog(z + "/" + z1);
                            if (Math.Abs(z - z1) == 2) allowed = false;
                        }
                        break;//Chỉnh sửa ngày 28-03-2020
                    }
                }
            }
            if (z2 >= -7)
            {
                Photon_Chess chess_f = Photon_Match_Spawning.control.Find_Chess(new Vector3(x, y, z2));
                if (chess_f != null)
                {
                    if (chess_f.chess_handler.myTeam == current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                    {
                        break;
                    }
                    else
                    {
                        if (chess_f.chess_handler.chess_type != Chess_Type.Pawn && chess_f.chess_handler.chess_type != Chess_Type.Knight && chess_f.chess_handler.chess_type != Chess_Type.Bishop)
                            allowed = false;
                        else if (chess_f.chess_handler.chess_type == Chess_Type.Pawn)
                        {
                            Photon_Debug.instance.AddDebugLog(z + "/" + z2);
                            if (Math.Abs(z - z2) == 2) allowed = false;
                        }
                        break;//Chỉnh sửa ngày 28-03-2020
                    }
                }
            }
        }
        return allowed;
    }

    //Check tất cả các quân theo chiều xiên (bỏ qua quân tốt => vì tốt di chuyển 1 ô nên xét ở hàm khác, mã, vua, xe vì những quân này ko di chuyển theo chiều xiên nên ko thể bị chiếu)
    protected bool Check_Surround(GameObject current_chess, int x, int y, int z)
    {
        bool allowed = true;
        int multiplier = 0;
        Photon_Chess chess;
        bool continued_one = true, continued_two = true, continued_three = true, continued_four = true;//4 biến tượng trưng cho 4 lượt xét chéo xung quanh quân cờ
        for (int i = 0; i < 8; i++)
        {
            multiplier++;
            float x1 = x + multiplier * 2;
            float z1 = z + multiplier * 2;
            ///
            float x2 = x - multiplier * 2;
            float z2 = z - multiplier * 2;
            ///

            if (continued_one)
            {
                if (x1 >= -7 && x1 <= 7 && z1 >= -7 && z1 <= 7)
                {
                    chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x1, y, z1));
                    if (chess != null)
                    {
                        if (chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                        {
                            if (chess.chess_handler.chess_type != Chess_Type.Pawn &&
                                chess.chess_handler.chess_type != Chess_Type.Knight &&
                                chess.chess_handler.chess_type != Chess_Type.King &&
                                chess.chess_handler.chess_type != Chess_Type.Rook)
                                allowed = false;
                            else continued_one = false;
                        }
                        else continued_one = false;
                    }
                }
            }
            if (continued_two)
            {
                if (x2 >= -7 && x2 <= 7 && z1 >= -7 && z1 <= 7)
                {
                    chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x2, y, z1));
                    if (chess != null)
                    {
                        if (chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                        {
                            if (chess.chess_handler.chess_type != Chess_Type.Pawn &&
                                chess.chess_handler.chess_type != Chess_Type.Knight &&
                                chess.chess_handler.chess_type != Chess_Type.King &&
                                chess.chess_handler.chess_type != Chess_Type.Rook)
                                allowed = false;
                            else continued_two = false;
                        }
                        else continued_two = false;
                    }
                }
            }
            if (continued_three)
            {
                if (x1 >= -7 && x1 <= 7 && z2 >= -7 && z2 <= 7)
                {
                    chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x1, y, z2));
                    if (chess != null)
                    {
                        if (chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                        {
                            if (chess.chess_handler.chess_type != Chess_Type.Pawn &&
                                chess.chess_handler.chess_type != Chess_Type.Knight &&
                                chess.chess_handler.chess_type != Chess_Type.King &&
                                chess.chess_handler.chess_type != Chess_Type.Rook)
                                allowed = false;
                            else continued_three = false;
                        }
                        else continued_three = false;
                    }
                }
            }
            if (continued_four)
            {
                if (x2 >= -7 && x2 <= 7 && z2 >= -7 && z2 <= 7)
                {
                    chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(x2, y, z2));
                    if (chess != null)
                    {
                        if (chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                        {
                            if (chess.chess_handler.chess_type != Chess_Type.Pawn &&
                                chess.chess_handler.chess_type != Chess_Type.Knight &&
                                chess.chess_handler.chess_type != Chess_Type.King &&
                                chess.chess_handler.chess_type != Chess_Type.Rook)
                                allowed = false;
                            else continued_four = false;
                        }
                        else continued_four = false;
                    }
                }
            }
        }
        return allowed;
    }

    //Check tất cả quân ô mà quân mã có thể đi xung quanh ô đang xét
    protected bool Check_Knight_Surround(GameObject current_chess, int x, int y, int z)
    {
        Photon_Chess chess;
        bool allowed = true;
        int[] X = new int[8] { 2, -2, -2, 2, -4, 4, -4, 4 };
        int[] Z = new int[8] { 4, 4, -4, -4, 2, 2, -2, -2 };

        for (int i = 0; i < 8; i++)
        {
            float u = x + X[i];
            float v = z + Z[i];

            if (u >= -7 && u <= 7 && v >= -7 && v <= 7)//Xét trong phạm vi bàn cờ
            {
                chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(u, y, v));
                if (chess != null)
                {
                    if (chess.chess_handler.myTeam != current_chess.GetComponent<Photon_Chess>().chess_handler.myTeam)
                    {
                        if (chess.chess_handler.chess_type == Chess_Type.Knight)
                        {
                            allowed = false;
                            break;
                        }
                    }
                }
            }
        }
        return allowed;
    }
    #endregion



    #region Photon Step
    /// <summary>
    /// Lưu lại các nước đã đi theo dạng chuỗi và send đến server
    /// </summary>
    protected void Send_Step_Storage(Photon_RPC photon_RPC, PhotonView PV, string step_storage)
    {
        photon_RPC = new Photon_RPC(PV);
        photon_RPC.Save_Step(step_storage);
    }

    /// <summary>
    /// Cập nhật quyền cho người chơi (ai được phép đi), gửi lên server
    /// </summary>
    protected void Send_Team_Permission(Photon_RPC photon_RPC, PhotonView PV, int permission)
    {
        photon_RPC = new Photon_RPC(PV);
        photon_RPC.Send_Permission(permission);
    }
    #endregion
}
